var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class vehicleHandler{
    constructor(){

    }

    createVehicle(req, res) {
        // if (!PermissionManager.canExecute(Permissions.CUSTOMERS_ADD, req, res)) { //Add the proper permission
        //     return HttpException.forbidden(res);
        // }

        var db = new Database(configuration.read().db);

        var vehicle = req.body;
        var params = [req.session.store.id, vehicle.storeId,vehicle.regNumber,vehicle.type,vehicle.brand,vehicle.model,vehicle.YoM,vehicle.remarks,vehicle.isActive];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createVehicle(?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been created.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}
module.exports = new vehicleHandler();