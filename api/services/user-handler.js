var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js');
    HashString = require('../../core/hashstring.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class UserHandler {
    constructor() {

    }

    register(req,res){
        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `CALL createNewUser(?,?,?,?,?)`;
        
        var hash = new HashString(),
            salt = hash.generateSalt(),
            hashedPassword = hash.createHash(req.body.password, salt);

        db.execute(query,[req.body.store, req.body.username, hashedPassword, salt,req.body.roleId]).then((recordSet) => {
            res.status(200).json(recordSet);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getPermissions(req,res){
        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `SELECT r.name AS 'role', GROUP_CONCAT(p.code) AS 'permissions' 
            FROM user u LEFT JOIN role r 
            ON u.roleId = r.id LEFT JOIN permission p ON p.roleId = u.roleId 
            WHERE u.storeId = ? AND u.username = ?`;

        db.query(query,[req.session.store.id, req.session.username]).then((recordSet) => {
            var result = recordSet.first();

            var userPermission = {
                role : result.role,
                permissions : result.permissions ? result.permissions.split(',') : []
            };

            res.status(200).json(userPermission);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new UserHandler();