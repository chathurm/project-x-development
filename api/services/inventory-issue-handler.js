var async = require('async'),
    configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class InventoryIssueHandler {
    constructor() {

    }

    createInventoryIssue(req, res) {
        var db = new Database(configuration.read().db);

        var inventoryIssue = req.body;

        var inventoryIssueParams = [req.session.store.id, inventoryIssue.employeeId, inventoryIssue.issuedDate,
        inventoryIssue.otherInfo, inventoryIssue.total, req.session.username];

        var inventoryIssueId = null;
        var inventoryIssueCode = null;

        db.beginTransaction().then(function () {

            db.execute('CALL createInventoryIssue(?,?,?,?,?,?)', inventoryIssueParams).then((result) => {
                inventoryIssueId = result.first().inventoryIssueId;
                inventoryIssueCode = result.first().inventoryIssueCode;

                async.eachSeries(inventoryIssue.items, function iterate(inventoryIssueItem, callback) {

                    var issuedItemParams = [inventoryIssueId, inventoryIssueItem.inventoryId, req.session.store.id, inventoryIssueItem.price,
                        inventoryIssueItem.quantity, inventoryIssueItem.subTotal, inventoryIssueItem.isSample, inventoryIssueItem.collectionDate];

                    db.execute('CALL addInventoryIssueItem(?,?,?,?,?,?,?,?)', issuedItemParams).then((result) => {
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                }, function done(err) {
                    if (err) {
                        //rollback
                        db.rollback().catch((error) => {
                            return HttpException.internalServerError(res, error);
                        });
                        return HttpException.internalServerError(res, err);
                    } else {
                        // commmit here when all the insertions have been successful
                        db.commit().then((result) => {
                            res.status(200).json({ success: true, inventoryIssueId: inventoryIssueId, inventoryIssueCode: inventoryIssueCode, message: 'Record has been saved.' });
                        }).catch((error) => {
                            return HttpException.internalServerError(res, err);
                        });
                    }
                });
            }).catch((err) => {
                db.close();
                return HttpException.internalServerError(res, err);
            });
        }).catch((err) => {
            db.close();
            return HttpException.internalServerError(res, err);
        });
    }

    getInventoryIssueDetails(req, res) {
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var params = [req.session.store.id, decodeURIComponent(req.params.inventoryIssueId)];

        var inventoryIssue = { inventoryIssueData: null, inventoryIssueItems: [] }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(`CALL getInventoryIssueDetails(${req.session.store.id},${req.params.inventoryIssueId}); CALL getInventoryIssueItems(${req.session.store.id},${req.params.inventoryIssueId})`).then((result) => {
            inventoryIssue.inventoryIssueData = result[0] ? result[0] : null;
            inventoryIssue.inventoryIssueItems = result[2] ? result[2] : [];
            res.send(inventoryIssue);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateItemTrackingStatus(req, res) {
        let db = new Database(configuration.read().db);

        let itemDeliveries = req.body;

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let updateQuery = `
        UPDATE	itemDeliveryTracker
        SET		itemOrderedAs = ?, status = ?, updatedDate = ?, updatedBy = ?
        WHERE	storeId = ? AND id = ?`;

        let insertQuery = `
        INSERT INTO itemDeliveryTracker
        (storeId, status, itemOrderedAs, createdDate, createdBy)
        VALUES (?,?,?,?,?)`;

        async.forEach(itemDeliveries, function (item, callback) {
            if (item.id) {
                db.query(updateQuery, [item.itemOrderedAs, item.orderStatus, new Date(), req.session.username, req.session.store.id, item.id]).then((result) => {
                    callback();
                }).catch((err) => {
                    return callback(err);
                });
            } else {
                db.query(insertQuery, [req.session.store.id, item.orderStatus, item.itemOrderedAs, new Date(), req.session.username]).then((result) => {
                    callback();
                }).catch((err) => {
                    return callback(err);
                });
            }
        }, function (err) {
            if (err) return HttpException.internalServerError(req, err);
            res.status(200).json({ success: true, message: 'Records have been updated.' });
        });

        db.close();
    }

    saveLoadingSheet(req, res) {
        let db = new Database(configuration.read().db);

        let loadingSheet = req.body;

        let loadingSheetParams = [req.session.store.id, loadingSheet.employeeId, loadingSheet.lineId, loadingSheet.loadingDate, loadingSheet.vehicleNo, loadingSheet.otherInfo, req.session.username];

        let loadingSheetId = null;
        let loadingSheetCode = null;

        let updateQuery = `
        UPDATE	itemDeliveryTracker
        SET		itemOrderedAs = ?, status = ?, updatedDate = ?, updatedBy = ?, loadingSheetId = ?
        WHERE	storeId = ? AND id = ?`;

        let insertQuery = `
        INSERT INTO itemDeliveryTracker
        (storeId, status, loadingSheetId, customerId, itemId, itemOrderedAs, createdDate, createdBy)
        VALUES (?,?,?,?,?,?,?,?)`;

        db.beginTransaction().then(function () {
            async.series([
                //create loading sheet entry
                function (callback) {
                    db.execute('CALL createLoadingSheet(?,?,?,?,?,?,?)', loadingSheetParams).then((result) => {
                        loadingSheetId = result.first().loadingSheetId;
                        loadingSheetCode = result.first().loadingSheetCode;
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                },
                //add loading Items
                function (callback) {
                    async.forEach(loadingSheet.items, function (item, eachCallback) {
                        if (item.id) {
                            db.query(updateQuery, [item.itemOrderedAs, item.orderStatus, new Date(), req.session.username, loadingSheetId, req.session.store.id, item.id]).then((result) => {
                                eachCallback();
                            }).catch((err) => {
                                return eachCallback(err);
                            });
                        } else {
                            db.query(insertQuery, [req.session.store.id, item.orderStatus, loadingSheetId, item.customerId, item.itemId, item.itemOrderedAs, new Date(), req.session.username]).then((result) => {
                                eachCallback();
                            }).catch((err) => {
                                return eachCallback(err);
                            });
                        }
                    }, function (err) {
                        if (err) {
                            return callback(err);
                        } else {
                            callback();
                        }
                    });
                }
            ], function (err) { //This function gets called after all the callbacks called
                if (err) {
                    //rollback
                    db.rollback().catch((error) => {
                        return HttpException.internalServerError(res, error);
                    });
                    return HttpException.internalServerError(res, err);
                } else {
                    // commmit here when all the insertions have been successful
                    db.commit().then((result) => {
                        res.status(200).json({ success: true, loadingSheetId: loadingSheetId, loadingSheetCode: loadingSheetCode, message: 'Record has been saved.' });
                    }).catch((error) => {
                        return HttpException.internalServerError(res, err);
                    });
                }
                db.close();
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });
    }

    getItemListByTackingStatus(req, res) {
        const db = new Database(configuration.read().db);

        const params = [req.session.store.id, req.params.status, req.session.store.id, req.params.status];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        const query = `
        SELECT 
            idt.id,
            c.customerId,
            c.name AS 'customerName',
            p.amount AS 'customProductPrice',
            p.amount AS 'collectiveAmount',
            pi.itemId,
            pi.quantity,
            idt.itemOrderedAs,
            i.code AS 'itemCode',
            i.name AS 'itemName',
            cpi.label AS 'compositeItemLabel',
            1 AS 'isSelected'
        FROM
            productItem pi
            LEFT JOIN compositeProductItem cpi ON pi.compositeProductItemId = cpi.id
            LEFT JOIN itemDeliveryTracker idt ON idt.compositeItemId = pi.compositeProductItemId
            INNER JOIN item i ON pi.itemId = i.id
            INNER JOIN product p ON cpi.productId = p.id
            LEFT JOIN customer c ON c.id = idt.customerId
        WHERE	idt.storeId = ? AND idt.status = ?
        
        UNION

        SELECT 
            idt.id, c.customerId, c.name AS 'customerName',
            0 AS 'customProductPrice', 0 AS 'collectiveAmount',
            idt.itemId, idt.quantity, idt.itemOrderedAs, i.code AS 'itemCode',
            i.name AS 'itemName', '' AS 'compositeItemLabel', 1 AS 'isSelected'
        FROM
            itemDeliveryTracker idt LEFT JOIN
            item i ON idt.itemId = i.id LEFT JOIN
            customer c ON c.id = idt.customerId
        WHERE idt.storeId = ? AND idt.status = ? AND idt.compositeItemId IS NULL`;

        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getAllItemList(req, res) {
        let db = new Database(configuration.read().db);

        let params = [req.session.store.id, req.session.store.id];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `
        SELECT 
            idt.id,
            c.customerId,
            c.name AS 'customerName',
            p.amount AS 'customProductPrice',
            p.amount AS 'collectiveAmount',
            pi.itemId,
            pi.quantity,
            idt.itemOrderedAs,
            i.code AS 'itemCode',
            i.name AS 'itemName',
            1 AS 'isSelected'
        FROM
            itemDeliveryTracker idt
                INNER JOIN
            productItem pi ON idt.customProductItemId = pi.id
                INNER JOIN
            item i ON pi.itemId = i.id
                LEFT OUTER JOIN
            compositeProductItem cpi ON pi.compositeProductItemId = cpi.id
                LEFT OUTER JOIN
            product p ON cpi.productId = p.id
                LEFT JOIN
            customerProduct cp ON cp.productId = p.id
                LEFT JOIN
            customer c ON c.id = cp.customerId
        WHERE	idt.storeId = ?
        
        UNION

        SELECT 
            idt.id, c.customerId, c.name AS 'customerName',
            0 AS 'customProductPrice', 0 AS 'collectiveAmount',
            idt.itemId, idt.quantity, idt.itemOrderedAs, i.code AS 'itemCode',
            i.name AS 'itemName', 1 AS 'isSelected'
        FROM
            itemDeliveryTracker idt LEFT JOIN
            item i ON idt.itemId = i.id LEFT JOIN
            customer c ON c.id = idt.customerId
        WHERE idt.storeId = ? AND idt.customProductItemId IS NULL`;

        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchInventoryIssues(req, res) {

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchInventoryIssues(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchInventoryIssues(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

};

module.exports = new InventoryIssueHandler();