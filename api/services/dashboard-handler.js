var Configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class DashboardHandler {
    constructor() {

    }

    getOutOfStockItems (req, res){
        var config = Object.assign({
            multipleStatements: true
        }, Configuration.read().db);

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });     

        var pageSize = typeof req.query.size === 'undefined' ? 10 : parseInt(decodeURIComponent(req.query.size));
        var index = typeof req.query.index === 'undefined' ? 1 : parseInt(decodeURIComponent(req.query.index));
        
        index = index < 0 ? 0 : index - 1;

        var query = `SELECT *
                        FROM reorderItemsView
                        WHERE storeId=${req.session.store.id}
                        LIMIT ${index}, ${pageSize};

                        SELECT count(itemId) as count
                        FROM reorderItemsView
                        WHERE storeId=${req.session.store.id};`;

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[1][0].count });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new DashboardHandler();