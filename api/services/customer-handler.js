var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class CustomerHandler {
    constructor() {

    }

    createCustomer(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var customer = req.body;
        var params = [req.session.store.id, customer.lineId, customer.name, customer.nic, customer.email, customer.address, customer.mobile1, customer.mobile2,
        customer.fixedLine, customer.emergencyContact, customer.contactName, customer.language, customer.latitude, customer.longitude, customer.otherInfo, customer.imageUrl];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createCustomer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, customerId: result.first().$customerId, message: 'Record has been created.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCustomerByCustomerId(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getCustomerByCustomerId(?, ?)', [decodeURIComponent(req.params.customerId), req.session.store.id]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deactivateCustomerById(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_CHANGE_STATUS, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateCustomerActiveStatus(?,?)', [decodeURIComponent(req.params.customerId), req.body.isActive]).then((result) => {
            res.status(200).json({ success: true, message: 'Record deactivated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deleteCustomerById(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_DELETE, req, res)) {
            return HttpException.forbidden(res);
        }
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateCustomerDeleteStatus(?)', [decodeURIComponent(req.params.customerId)]).then((result) => {
            res.status(200).json({ success: true, message: 'Record deleted.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateCustomer(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_EDIT, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var customer = req.body;
        var params = [customer.id, req.session.store.id, customer.lineId, customer.name, customer.nic, customer.email, customer.address,
        customer.mobile1, customer.mobile2, customer.fixedLine, customer.emergencyContact, customer.contactName, customer.language, customer.latitude, customer.longitude,
        customer.otherInfo, customer.imageUrl];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateCustomer(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAllCustomers(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_SEARCH, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAllCustomers(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchAllCustomers(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getRecentCreatedList(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_SEARCH, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);
        var size = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute(`CALL customerGetRecentList (?,?,?)`, [req.session.store.id, size, configuration.read().grid.pageSize]).then((result) => {
            res.status(200).json({ records: result });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    lookup(req, res) {
        if (!PermissionManager.canExecute(Permissions.CUSTOMERS_SEARCH, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAllCustomers(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchAllCustomers(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            var records = recordSet.first().map(function (c) {
                return {
                    id: c.id,
                    text: c.name
                };
            });

            res.status(200).json({ records: records, recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new CustomerHandler();