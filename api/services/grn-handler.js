var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions,
    async = require('async');

class GrnHandler {
    constructor() {

    }

    createGrn(req, res) {
        if (!PermissionManager.canExecute(Permissions.GRN_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var grn = req.body;

        //calculate total amount
        var totalAmount = 0;
        grn.inventoryItems.forEach(function (item) {
            totalAmount += item.buyingPrice * item.quantity;
        });

        var params = [req.session.store.id, grn.grnCode, grn.description, grn.receivedDate, totalAmount, grn.discountAmount, grn.supplier, grn.paymentMethodId,
        grn.chequeNo, grn.bank, grn.branch, req.session.username];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var grnId = null;
        var grnCode = null;

        db.beginTransaction().then(function () {

            async.series([
                //create grn entry
                function (callback) {
                    db.execute('CALL createGrn(?,?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
                        grnId = result[0].$grnId;
                        grnCode = grn.grnCode;
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                },
                //add inventory items
                function (callback) {
                    async.forEachSeries(grn.inventoryItems, function (inventoryItem, eachCallback) {
                        var inventoryItemparams = [req.session.username, req.session.store.id, inventoryItem.itemId, grnId, inventoryItem.itemImageUrl, inventoryItem.quantity, inventoryItem.buyingPrice, inventoryItem.customPrice,
                        inventoryItem.retailSellingPrice, inventoryItem.wholeSellingPrice, inventoryItem.easyPaymentSellingPrice];

                        db.execute('CALL createInventoryItem(?,?,?,?,?,?,?,?,?,?,?)', inventoryItemparams).then((result) => {
                            callback();
                        }).catch((err) => {
                            return eachCallback(err);
                        });
                    }, function (err) {
                        if (err) {
                            return callback(err);
                        } else {
                            callback();
                        }
                    });
                },
                //add transactions
                function (callback) {
                    let transactionParams = ['GRN', null, grnId, req.session.store.id, totalAmount, 0, req.session.username];
                    db.execute('CALL addTransaction(?,?,?,?,?,?,?)', transactionParams).then((result) => {
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                }
            ], function (err) { //This function gets called after all the callbacks called
                if (err) {
                    //rollback
                    db.rollback().catch((error) => {
                        return HttpException.internalServerError(res, error);
                    });
                    return HttpException.internalServerError(res, err);
                } else {
                    // commmit here when all the insertions have been successful
                    db.commit().then((result) => {
                        res.status(200).json({ success: true, message: 'Record has been created.' });
                    }).catch((error) => {
                        return HttpException.internalServerError(res, err);
                    });
                }
                db.close();
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });
    }

    getGRNDetails(req, res) {
        if (!PermissionManager.canExecute(Permissions.GRN_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var params = [req.session.store.id, decodeURIComponent(req.params.grnId)];

        var grn = { grnData: null, inventoryItems: [] }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(`CALL getGRNDetails(${req.session.store.id},${decodeURIComponent(req.params.grnId)}); CALL getInventoryItems(${req.session.store.id},${decodeURIComponent(req.params.grnId)})`).then((recordSet) => {
            grn.grnData = recordSet[0] ? recordSet[0] : null;
            grn.inventoryItems = recordSet[2] ? recordSet[2] : [];
            res.send(grn);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAllGRNItems(req, res) {
        if (!PermissionManager.canExecute(Permissions.GRN_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchGRN(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchGRN(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new GrnHandler();