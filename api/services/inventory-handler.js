var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class InventoryHandler {
    constructor() {

    }

    getInventoryItemByItemId(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getInventoryItemById(?,?)', [req.session.store.id, decodeURIComponent(req.params.inventoryId)]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateInventoryItem(req, res) {
        if (!PermissionManager.canExecute(Permissions.INVENTORY_EDIT, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var inventoryItem = req.body;
        var params = [inventoryItem.inventoryId, req.session.username, req.session.store.id, inventoryItem.itemId, inventoryItem.itemName, inventoryItem.itemImageUrl, inventoryItem.quantity, inventoryItem.buyingPrice,
        inventoryItem.retailSellingPrice, inventoryItem.wholeSellingPrice, inventoryItem.easyPaymentSellingPrice];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateInventoryItem(?,?,?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchInventory(req, res) {
        if (!PermissionManager.canExecute(Permissions.GRN_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var itemId = req.query.itemId === undefined ? null : decodeURIComponent(req.query.itemId);
        var startDate = req.query.startDate === undefined ? null : decodeURIComponent(req.query.startDate);
        var endDate = req.query.endDate === undefined ? null : decodeURIComponent(req.query.endDate);
        var pageSize = req.query.size === undefined ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === undefined ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchInventory(${req.session.store.id}, ${itemId}, '${searchText}', ${startDate}, ${endDate}, ${index}, ${pageSize}, @recordCount); SELECT @recordCount`;
        }
        else {
            query = `CALL searchInventory(${req.session.store.id}, ${itemId}, ${searchText}, ${startDate}, ${endDate}, ${index}, ${pageSize}, @recordCount); SELECT @recordCount`;
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAvailableInventoryItems(req, res) {
        var db = new Database(configuration.read().db);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL searchAvailableInventory(?,?)', [req.session.store.id, searchText]).then((result) => {
            res.status(200).json({ records: result });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAvailableStock(req, res) {
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === undefined ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === undefined ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAvailableStock(${req.session.store.id}, '${searchText}', ${index}, ${pageSize}, @recordCount); SELECT @recordCount`;
        }
        else {
            query = `CALL searchAvailableStock(${req.session.store.id}, ${searchText}, ${index}, ${pageSize}, @recordCount); SELECT @recordCount`;
        }

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    /*
    Returned item related functions
    */
    createReturnedItem(req, res) {
        //TODO: Change the permission type
        // if (!PermissionManager.canExecute(Permissions.ITEM_ADD, req, res)) {
        //     return HttpException.forbidden(res);
        // }

        var db = new Database(configuration.read().db);

        var returnedItem = req.body;
        var params = [req.session.store.id, returnedItem.itemId, returnedItem.itemName, returnedItem.customerId, req.session.username];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL addReturnedItem(?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been added.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
            console.log(err);
        });

        db.close();
    }

    searchAllSoldItems(req, res) {
        //TODO: Change the permission type
        // if (!PermissionManager.canExecute(Permissions.ITEM_ADD, req, res)) {
        //     return HttpException.forbidden(res);
        // }

        var db = new Database(configuration.read().db);

        var salesType = req.params.salesType;
        var itemId = req.params.itemId;
        var customerId = decodeURIComponent(req.query.customerId);

        var params = [req.session.store.id, salesType, itemId, customerId];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });


        db.execute('CALL searchAllSoldItemsForACustomer(?,?,?,?)', params).then((result) => {
            res.status(200).json(result.first());
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getInventoriesByItemId(req, res) {
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var itemId = req.query.itemId === undefined ? null : decodeURIComponent(req.query.itemId);
        var startDate = req.query.startDate === undefined ? null : decodeURIComponent(req.query.startDate);
        var endDate = req.query.endDate === undefined ? null : decodeURIComponent(req.query.endDate);
        var pageSize = req.query.size === undefined ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === undefined ? null : decodeURIComponent(req.query.index);

        var query = null;
        query = `CALL getInventoriesByItemId(${req.session.store.id}, ${itemId}, ${startDate}, ${endDate}, ${index}, ${pageSize}, @recordCount); SELECT @recordCount`;

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new InventoryHandler();