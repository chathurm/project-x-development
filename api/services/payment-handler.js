var async = require('async'),
    configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class PaymentHandler {
    constructor() {

    }

    createPayment(req, res) {
        if (!PermissionManager.canExecute(Permissions.PAYMENT_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var payment = req.body;

        var paymentParams = [req.session.store.id, payment.customerId, payment.paymentMethodId, payment.paymentDate, payment.amount, payment.otherInfo, req.session.username];

        var paymentId = null;
        var paymentCode = null;

        db.beginTransaction().then(function () {

            async.series([
                //create payment entry
                function (callback) {
                    db.execute('CALL createPayment(?,?,?,?,?,?,?)', paymentParams).then((result) => {
                        paymentId = result.first().paymentId;
                        paymentCode = result.first().paymentCode;
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                },
                //add transactions
                function (callback) {
                    async.forEachSeries(payment.items, function (paymentItem, eachCallback) {
                        if (paymentItem.amount > 0) {
                            let paymentItemParams = ['', paymentId, paymentItem.id, req.session.store.id, paymentItem.amount, 1, req.session.username];
                            db.execute('CALL addTransaction(?,?,?,?,?,?,?)', paymentItemParams).then((result) => {
                                eachCallback();
                            }).catch((err) => {
                                return eachCallback(err);
                            });
                        } else {
                            eachCallback();
                        }
                    }, function (err) {
                        if (err) {
                            return callback(err);
                        } else {
                            callback();
                        }
                    });
                },
                //add cheques if available
                function (callback) {
                    if (payment.cheques.length > 0) {
                        async.forEachSeries(payment.cheques, function (chequeItem, eachCallback) {
                            let paymentChequeParams = [req.session.store.id, paymentId, chequeItem.chequeNo, chequeItem.bank, chequeItem.branch, chequeItem.chequeDate, chequeItem.amount];
                            db.execute('CALL addCheque(?,?,?,?,?,?,?)', paymentChequeParams).then((result) => {
                                eachCallback();
                            }).catch((err) => {
                                return eachCallback(err);
                            });
                        }, function (err) {
                            if (err) {
                                return callback(err);
                            } else {
                                callback();
                            }
                        });
                    } else {
                        callback();
                    }
                }
            ], function (err) { //This function gets called after all the callbacks called
                if (err) {
                    //rollback
                    db.rollback().catch((error) => {
                        return HttpException.internalServerError(res, error);
                    });
                    return HttpException.internalServerError(res, err);
                } else {
                    // commmit here when all the insertions have been successful
                    db.commit().then((result) => {
                        res.status(200).json({ success: true, paymentId: paymentId, paymentCode: paymentCode, message: 'Record has been saved.' });
                    }).catch((error) => {
                        return HttpException.internalServerError(res, err);
                    });
                }
                db.close();
            });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });
    }

    getPendingPayments(req, res) {
        if (!PermissionManager.canExecute(Permissions.PAYMENT_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `SELECT	id, code AS 'invoiceCode', DATE_FORMAT(invoicedDate,'%Y-%m-%d') AS 'invoicedDate', netAmount AS 'total', dueAmount, 0 AS 'amount'
                     FROM	invoice
                     WHERE	storeId = ? AND customerId = ? AND dueAmount > 0`;

        db.query(query, [req.session.store.id, req.params.customerId]).then((recordSet) => {
            res.status(200).json(recordSet);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getPaymentDetails(req, res) {
        if (!PermissionManager.canExecute(Permissions.PAYMENT_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var params = [req.session.store.id, decodeURIComponent(req.params.paymentId)];

        var payment = { paymentData: null, paymentItems: [], paymentCheques: [] }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `
        SELECT	p.id, p.code, p.customerId, CONCAT(c.customerId , ' - ' , c.name) AS 'customerName', p.amount,
			    p.paymentMethod, (SELECT name FROM lookup WHERE id = p.paymentMethod) AS 'paymentMethodName',
                DATE_FORMAT(p.paymentDate,'%Y-%m-%d') AS 'paymentDate', p.otherInfo
	    FROM	payment p LEFT JOIN	customer c ON c.id = p.customerId
	    WHERE	p.storeId = ${req.session.store.id} AND p.id = ${req.params.paymentId};
        
        SELECT	i.code AS 'invoiceCode', i.invoicedDate, i.netAmount AS 'total', t.amount, i.dueAmount
        FROM	transaction t LEFT JOIN invoice i ON t.referenceId = i.id
        WHERE	t.storeId = ${req.session.store.id} AND t.paymentId = ${req.params.paymentId};

        SELECT	chequeNo, bank, branch, chequeDate, amount
        FROM	paymentCheques
        WHERE	storeId = ${req.session.store.id} AND paymentId = ${req.params.paymentId};
        `

        db.query(query).then((result) => {
            payment.paymentData = result[0] ? result[0] : null;
            payment.paymentItems = result[1] ? result[1] : [];
            payment.paymentCheques = result[2] ? result[2] : [];
            res.send(payment);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deletePayment(req, res) {
        if (!PermissionManager.canExecute(Permissions.PAYMENT_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var params = [req.session.store.id, decodeURIComponent(req.params.paymentId), req.session.username];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL deletePayment(?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchPayments(req, res) {
        if (!PermissionManager.canExecute(Permissions.PAYMENT_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchPayments(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchPayments(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new PaymentHandler();