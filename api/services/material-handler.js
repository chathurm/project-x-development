var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class MaterialHandler {
    constructor() {

    }

    all(req, res){
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let keyword = req.query.keyword;
        let query = `SELECT
            m.id,
            m.name,
            m.description,
            m.isActive,
            m.validFrom,
            m.validTo,
            m.isNeverExpire
        FROM material m
        ORDER BY m.validFrom DESC, m.validTo, m.name
        WHERE m.storeId = ${req.session.store.id} AND ${keyword} is null OR m.name like CONCAT(${keyword},'%');`;

        db.query(query).then((result) => {
            res.status(200).json(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    create(req, res){
        // TO DO : Add the permission 

        // if (!PermissionManager.canExecute(Permissions.LINE_ADD, req, res)) {
        //     return HttpException.forbidden(res);
        // }
        var db = new Database(configuration.read().db);

        var material = req.body;
        var params = [
            req.session.store.id, 
            material.name,
            material.isActive,
            material.description,
            material.validFrom,
            material.validTo,
            material.isNeverExpire,
            req.session.username
        ];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createMaterial(?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true,  message: 'Record has been created.' });

        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    lookup(req, res) {
        // TO DO : Add the permission

        // if (!PermissionManager.canExecute(Permissions.MATERIAL_SEARCH, req, res)) {
        //     return HttpException.forbidden(res);
        // }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchAllMaterials(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchAllMaterials(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            var records = recordSet.first().map(function (m) {
                return {
                    id: m.id,
                    text: m.name
                };
            });

            res.status(200).json({ records: records, recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new MaterialHandler();