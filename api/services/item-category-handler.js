var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class ItemCategoryHandler {
    constructor() {

    }

    getActiveItemCategories(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getActiveItemCategories(?)', [req.session.store.id]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getItemSubCategoriesByCategoryId(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getItemSubCategoriesByCategoryId(?,?)', [req.params.categoryId, req.session.store.id]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

}

module.exports = new ItemCategoryHandler();