var async = require('async'),
    configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class InvoiceHandler {
    constructor() {

    }

    createInvoice(req, res) {
        if (!PermissionManager.canExecute(Permissions.INVOICE_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var invoice = req.body;

        var invoiceParams = [req.session.store.id, invoice.customerId, invoice.otherInfo, invoice.salesTypeId, invoice.paymentMethodId, invoice.invoicedDate, invoice.dueDate,
        invoice.chequeNo, invoice.bank, invoice.branch, invoice.total, invoice.discountAmount, invoice.netAmount, invoice.receivedAmount, invoice.balanceAmount, invoice.dueAmount, req.session.username];

        var invoiceId = null;
        var invoiceCode = null;

        db.beginTransaction().then(function () {

            db.execute('CALL createInvoice(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', invoiceParams).then((result) => {
                invoiceId = result.first().invoiceId;
                invoiceCode = result.first().invoiceCode;

                async.eachSeries(invoice.items, function iterate(invoiceItem, callback) {

                    var invoiceItemParams = [invoiceId, invoiceItem.inventoryId, req.session.store.id, invoiceItem.price,
                        invoiceItem.quantity, invoiceItem.subTotal];

                    db.execute('CALL addInvoiceItem(?,?,?,?,?,?)', invoiceItemParams).then((result) => {
                        callback();
                    }).catch((err) => {
                        return callback(err);
                    });
                }, function done(err) {
                    if (err) {
                        //rollback
                        db.rollback().catch((error) => {
                            return HttpException.internalServerError(res, error);
                        });
                        return HttpException.internalServerError(res, err);
                    } else {
                        // commmit here when all the insertions have been successful
                        db.commit().then((result) => {
                            res.status(200).json({ success: true, invoiceId: invoiceId, invoiceCode: invoiceCode, message: 'Record has been saved.' });
                        }).catch((error) => {
                            return HttpException.internalServerError(res, err);
                        });
                    }
                });
            }).catch((err) => {
                db.close();
                return HttpException.internalServerError(res, err);
            });
        }).catch((err) => {
            db.close();
            return HttpException.internalServerError(res, err);
        });
    }

    getInvoiceDetails(req, res) {
        if (!PermissionManager.canExecute(Permissions.INVOICE_VIEW_DETAILS, req, res)) {
            return HttpException.forbidden(res);
        }

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var params = [req.session.store.id, decodeURIComponent(req.params.invoiceId)];

        var invoice = { invoiceData: null, invoiceItems: [] }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(`CALL getInvoiceDetails(${req.session.store.id},${decodeURIComponent(req.params.invoiceId)}); CALL getInvoiceItems(${req.session.store.id},${decodeURIComponent(req.params.invoiceId)})`).then((result) => {
            invoice.invoiceData = result[0] ? result[0] : null;
            invoice.invoiceItems = result[2] ? result[2] : [];
            res.send(invoice);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateInvoiceDeleteStatus(req, res) {
        if (!PermissionManager.canExecute(Permissions.INVOICE_ADD, req, res)) {
            return HttpException.forbidden(res);
        }

        var db = new Database(configuration.read().db);

        var params = [req.session.store.id, decodeURIComponent(req.params.invoiceId), req.session.username];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateInvoiceDeleteStatus(?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getPaymentDueInvoices(req, res) {
        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : req.query.size;
        var index = req.query.index === 'undefined' ? null : req.query.index;

        var query = null;

        if (searchText) {
            query = `CALL searchPaymentDueInvoices(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchPaymentDueInvoices(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchInvoices(req, res) {

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchInvoices(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchInvoices(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

}

module.exports = new InvoiceHandler();