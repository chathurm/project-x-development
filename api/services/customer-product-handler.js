var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class CustomerProductHandler {
    constructor() {

    }

    createCustomerProduct(req, res) {
        var db = new Database(configuration.read().db);

        var customerProduct = req.body;
        var params = [req.session.store.id, customerProduct.customerId, customerProduct.productId, customerProduct.employeeId, customerProduct.lineId, customerProduct.periodFrom,
        customerProduct.periodTo, customerProduct.otherInfo, req.session.username];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createCustomerProduct(?,?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, customProductItemId: result.first().$customProductItemId, message: 'Record has been created.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCustomerProductDetails(req, res) {
        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `SELECT 		
                cp.id, cp.code, cp.productId, p.name AS 'productName', cp.customerId, c.name AS 'customerName', cp.employeeId, e.name AS 'employeeName', cp.lineId, l.route AS 'lineRoute',
				            cp.otherInfo, DATE_FORMAT(periodFrom,'%Y-%m-%d') AS 'periodFrom', DATE_FORMAT(periodTo,'%Y-%m-%d') AS 'periodTo', cp.isActive
                FROM 		customerProduct cp
                            LEFT JOIN customer c ON c.id = cp.customerId
                            LEFT JOIN product p ON p.id = cp.productId
                            LEFT JOIN employee e ON e.id = cp.employeeId
                            LEFT JOIN line l ON l.id = cp.lineId
                WHERE		cp.id = ? AND cp.storeId = ?;`;

        let params = [req.params.customProductItemId, req.session.store.id];
        
        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCustomProductForCustomer(req, res) {
        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var params = [req.session.store.id, req.params.customerId];

        let query = `SELECT 
                        cp.productId, cp.code, p.name, p.amount
                    FROM
                        customerProduct cp
                            INNER JOIN
                        product p ON cp.productId = p.id
                    WHERE
                        cp.storeId = ? AND cp.customerId = ?
                            AND p.isDeleted = 0
                            AND (cp.periodFrom IS NULL
                            || cp.periodFrom <= SYSDATE())
                            AND (cp.periodTo IS NULL
                            || cp.periodTo >= SYSDATE())`;

        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getCompositeItemDeliveryStatus(req, res) {
        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let query = `SELECT idt.id AS trackingId, idt.compositeItemId,idt.term,idt.status,cpi.isFree
                    FROM itemDeliveryTracker idt
                        JOIN customerProduct cp ON idt.customerId = cp.customerId
                        JOIN compositeProductItem cpi on idt.compositeItemId = cpi.id
                    WHERE idt.storeId = ? AND cp.productId = ?
                    ORDER BY idt.term;`;

        var params = [req.session.store.id, req.params.productId];

        db.query(query, params).then((result) => {
            res.json(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getProductItemsForProduct(req, res) {
        var config = configuration.read().db;

        var db = new Database(config);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        var params = [req.session.store.id, req.params.productId];

        let query = `SELECT pi.id, pi.productId, pi.itemId, i.code, i.name, pi.price, pi.quantity
                    FROM productItem pi inner join item i on pi.itemId = i.id
                    WHERE pi.storeId = ? AND pi.productId = ?`;

        db.query(query, params).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    saveCustomProductToTerms(req, res) {
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        let params = null,
            command = req.body.command,
            query = null;

        switch (command) {
            case 'insert':
                let itemOrderedAs = req.body.isFree ? 'Free' : 'Default';

                query = 'INSERT INTO itemDeliveryTracker (storeId,compositeItemId,customerId, term,status,itemOrderedAs) VALUES(?,?,?,?,?,?)';
                params = [req.session.store.id, req.body.compositeItemId, req.body.customerId, req.body.termId, 1, itemOrderedAs];
                break;

            case 'remove':
                query = 'DELETE FROM itemDeliveryTracker WHERE storeId = ? AND id = ?';
                params = [req.session.store.id, req.body.trackingId];
                break;
        }
        
        db.query(query, params).then((result) => {
            return res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchCustomerProducts(req, res) {

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchCustomerProducts(${req.session.store.id},'${searchText}',${index},${pageSize},@recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchCustomerProducts(${req.session.store.id},${searchText},${index},${pageSize},@recordCount); SELECT @recordCount`;
        }
        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
};

module.exports = new CustomerProductHandler();