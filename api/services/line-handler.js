var configuration = require('../../core/config.js'),
    Database = require('../../core/database.js'),
    HttpException = require('../models/http-exception.js'),
    PermissionManager = require('../../core/permission.js').PermissionManager,
    Permissions = require('../../core/permission.js').Permissions;

class LineHandler {
    constructor() {

    }

    createLine(req, res) {
        if (!PermissionManager.canExecute(Permissions.LINE_ADD, req, res)) {
            return HttpException.forbidden(res);
        }
        var db = new Database(configuration.read().db);

        var line = req.body;
        var params = [req.session.store.id, req.session.username, line.route, line.code, line.lineDate, line.linerId, line.note];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL createLine(?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, lineId: result.first().$lineID, message: 'Record has been created.' });

        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getActiveLines(req, res) {
        if (!PermissionManager.canExecute(Permissions.LINE_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getAllActiveLines(?)', [req.session.store.id]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    getLineById(req, res) {
        if (!PermissionManager.canExecute(Permissions.LINE_VIEW, req, res)) {
            return HttpException.forbidden(res);
        }
        var db = new Database(configuration.read().db);

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL getLineById(?, ?)', [decodeURIComponent(req.params.lineId), req.session.store.id]).then((result) => {
            res.send(result);
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    updateLine(req, res) {
        if (!PermissionManager.canExecute(Permissions.LINE_EDIT, req, res)) {
            return HttpException.forbidden(res);
        }
        var db = new Database(configuration.read().db);

        var line = req.body;
        var params = [req.session.store.id, req.session.username, line.id, line.route, line.code, line.lineDate, line.linerId, line.note];

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL updateLine(?,?,?,?,?,?,?,?)', params).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    deleteLines(req, res) {
        if (!PermissionManager.canExecute(Permissions.LINE_DELETE, req, res)) {
            return HttpException.forbidden(res);
        }
        var db = new Database(configuration.read().db);

        var list = req.body.list;
        var listString = list.join(',');

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.execute('CALL deleteLines(?)', [listString]).then((result) => {
            res.status(200).json({ success: true, message: 'Record has been updated.' });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }

    searchAllLines(req, res) {

        var config = Object.assign({
            multipleStatements: true
        }, configuration.read().db);

        var db = new Database(config);

        var searchText = req.query.searchText === 'null' ? null : decodeURIComponent(req.query.searchText);
        var pageSize = req.query.size === 'undefined' ? null : decodeURIComponent(req.query.size);
        var index = req.query.index === 'undefined' ? null : decodeURIComponent(req.query.index);

        var query = null;

        if (searchText) {
            query = `CALL searchLines(${req.session.store.id}, '${searchText}', ${index}, ${pageSize}, @recordCount); SELECT @recordCount`;
        } else {
            query = `CALL searchLines(${req.session.store.id}, ${searchText}, ${index}, ${pageSize}, @recordCount); SELECT @recordCount`;
        }

        db.open().catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.query(query).then((recordSet) => {
            res.status(200).json({ records: recordSet.first(), recordCount: recordSet[2][0]["@recordCount"] });
        }).catch((err) => {
            return HttpException.internalServerError(res, err);
        });

        db.close();
    }
}

module.exports = new LineHandler();
