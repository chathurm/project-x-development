var express = require('express'),
    app = express(),
    Logger = require('../core/logger.js'),
    HttpException = require('./models/exception.js').HttpException,
    CustomerHandler = require('./services/customer-handler.js'),
    SupplierHandler = require('./services/supplier-handler.js'),
    LineHandler = require('./services/line-handler.js'),
    InventoryHandler = require('./services/inventory-handler.js'),
    ItemHandler = require('./services/item-handler.js'),
    Authentication = require('../core/auth/authentication.js'),
    UserHandler = require('./services/user-handler.js'),
    ItemCategoryHandler = require('./services/item-category-handler.js'),
    InvoiceHandler = require('./services/invoice-handler.js'),
    PaymentHandler = require('./services/payment-handler.js'),
    LookupHandler = require('./services/lookup-handler.js'),
    ProductHandler = require('./services/product-handler.js'),
    CustomerProductHandler = require('./services/customer-product-handler.js'),
    InventoryIssueHandler = require('./services/inventory-issue-handler.js'),
    DashboardHandler = require('./services/dashboard-handler.js'),
    EmployeeHandler = require('./services/employee-handler.js'),
    VehicleHandler = require('./services/vehicle-handler.js'),
    GrnHandler = require('./services/grn-handler'),
    MaterialHandler = require('./services/material-handler')

var auth = new Authentication(app);

app.get('/', (req, res, next) => {
    res.json({ text: 'hello' });
});

app.post('/customer/create', auth.authorize, CustomerHandler.createCustomer);
app.get('/customer/:customerId', auth.authorize, CustomerHandler.getCustomerByCustomerId);
app.put('/customer/status/:customerId', auth.authorize, CustomerHandler.deactivateCustomerById);
app.put('/customer/delete/:customerId', auth.authorize, CustomerHandler.deleteCustomerById);
app.put('/customer/update/:customerId', auth.authorize, CustomerHandler.updateCustomer);
app.get('/customers/search', auth.authorize, CustomerHandler.searchAllCustomers);
app.get('/customers/recently-created', auth.authorize, CustomerHandler.getRecentCreatedList);
app.get('/customers/lookup', auth.authorize, CustomerHandler.lookup);

app.post('/line/create', auth.authorize, LineHandler.createLine);
app.get('/line/:lineId', auth.authorize, LineHandler.getLineById);
app.put('/line/update/:lineId', auth.authorize, LineHandler.updateLine);
app.post('/line/delete', auth.authorize, LineHandler.deleteLines);
app.get('/lines/search', auth.authorize, LineHandler.searchAllLines);
app.get('/lines/active', auth.authorize, LineHandler.getActiveLines);

app.post('/supplier/create', auth.authorize, SupplierHandler.createSupplier);
app.get('/supplier/:supplierId', auth.authorize, SupplierHandler.getSupplierBySupplierId);
app.put('/supplier/status/:supplierId', auth.authorize, SupplierHandler.deactivateSupplierById);
app.put('/supplier/delete/:supplierId', auth.authorize, SupplierHandler.deleteSupplierById);
app.put('/supplier/update/:supplierId', auth.authorize, SupplierHandler.updateSupplier);
app.get('/suppliers/search', auth.authorize, SupplierHandler.searchAllSuppliers);
app.get('/suppliers/active', auth.authorize, SupplierHandler.getAllSuppliers);

app.post('/item/create', auth.authorize, ItemHandler.createItem);
app.get('/item/:itemId', auth.authorize, ItemHandler.getItemByItemId);
app.put('/item/status/:itemId', auth.authorize, ItemHandler.changeStatus);
app.put('/item/delete/:itemId', auth.authorize, ItemHandler.deleteItem);
app.put('/item/update/:itemId', auth.authorize, ItemHandler.updateItem);
app.get('/items/search', auth.authorize, ItemHandler.searchAllItems);
app.get('/items/search/active', auth.authorize, ItemHandler.searchActiveItems);
app.get('/items/recently-created', auth.authorize, ItemHandler.getRecentCreatedItemList);
app.get('/items/active', auth.authorize, ItemHandler.getActiveItems);

app.get('/categories/active', auth.authorize, ItemCategoryHandler.getActiveItemCategories);
app.get('/subcategories/:categoryId', auth.authorize, ItemCategoryHandler.getItemSubCategoriesByCategoryId);

app.put('/inventory/update/:itemId', auth.authorize, InventoryHandler.updateInventoryItem);
app.get('/inventory/:inventoryId', auth.authorize, InventoryHandler.getInventoryItemByItemId);
app.get('/inventories/search', auth.authorize, InventoryHandler.searchInventory);
app.get('/inventory/search/available', auth.authorize, InventoryHandler.searchAvailableInventoryItems);
app.get('/inventories/search/available', auth.authorize, InventoryHandler.searchAvailableInventoryItems);

app.post('/grn/create', auth.authorize, GrnHandler.createGrn);
app.get('/grn/search', auth.authorize, GrnHandler.searchAllGRNItems);
app.get('/grn/:grnId', auth.authorize, GrnHandler.getGRNDetails);

app.post('/user/register', UserHandler.register);
app.get('/user/permission', auth.authorize, UserHandler.getPermissions);

app.get('/:store/lookups.json', LookupHandler.getLookupsJson);
app.get('/lookups/category', auth.authorize, LookupHandler.getLookupCategory);
app.post('/lookups/category', auth.authorize, LookupHandler.createLookupCategory);
app.get('/lookups/category/:categoryId', auth.authorize, LookupHandler.getLookups);
app.post('/lookups', auth.authorize, LookupHandler.createLookup);

app.post('/invoice/create', auth.authorize, InvoiceHandler.createInvoice);
app.get('/invoices/search', auth.authorize, InvoiceHandler.searchInvoices);
app.get('/invoices/due/search', auth.authorize, InvoiceHandler.getPaymentDueInvoices);
app.get('/invoice/:invoiceId', auth.authorize, InvoiceHandler.getInvoiceDetails);
app.put('/invoice/delete/:invoiceId', auth.authorize, InvoiceHandler.updateInvoiceDeleteStatus);

app.get('/dashboard/outofstock-items', auth.authorize, DashboardHandler.getOutOfStockItems);

app.post('/product/create', auth.authorize, ProductHandler.createProduct);
app.get('/products/search', auth.authorize, ProductHandler.searchProducts);
app.get('/products/search/active', auth.authorize, ProductHandler.searchActiveProducts);
app.get('/product/:productId', auth.authorize, ProductHandler.getProductDetails);

app.post('/material/create',auth.authorize, MaterialHandler.create);
app.get('/material/list',auth.authorize, MaterialHandler.lookup);

app.post('/customer-product/create', auth.authorize, CustomerProductHandler.createCustomerProduct);
app.get('/customer-products/search', auth.authorize, CustomerProductHandler.searchCustomerProducts);
app.get('/customer-product/:customProductItemId', auth.authorize, CustomerProductHandler.getCustomerProductDetails);
app.get('/customer-product/customer/:customerId', auth.authorize, CustomerProductHandler.getCustomProductForCustomer);
app.post('/customer-product/:productId/terms/items', auth.authorize, CustomerProductHandler.saveCustomProductToTerms);
app.get('/customer-product/items/:productId', auth.authorize, CustomerProductHandler.getProductItemsForProduct);

app.get('/product/:productId/composite-items/delivery-status', auth.authorize, CustomerProductHandler.getCompositeItemDeliveryStatus);

app.post('/inventory-issue/create', auth.authorize, InventoryIssueHandler.createInventoryIssue);
app.post('/inventory-issue/loading-sheet', auth.authorize, InventoryIssueHandler.saveLoadingSheet);
app.get('/inventory-issues/search', auth.authorize, InventoryIssueHandler.searchInventoryIssues);
app.put('/inventory-issue/status', auth.authorize, InventoryIssueHandler.updateItemTrackingStatus);
app.get('/inventory-issue/:inventoryIssueId', auth.authorize, InventoryIssueHandler.getInventoryIssueDetails);
app.get('/inventory-issue/list/:status', auth.authorize, InventoryIssueHandler.getItemListByTackingStatus);
app.get('/inventory-issue/items/all', auth.authorize, InventoryIssueHandler.getAllItemList);

app.post('/payment/create', auth.authorize, PaymentHandler.createPayment);
app.get('/payments/search', auth.authorize, PaymentHandler.searchPayments);
app.get('/payment/:paymentId', auth.authorize, PaymentHandler.getPaymentDetails);
app.get('/payment/pending/:customerId', auth.authorize, PaymentHandler.getPendingPayments);
app.put('/payment/delete/:paymentId', auth.authorize, PaymentHandler.deletePayment);

app.get('/employees/search/active', auth.authorize, EmployeeHandler.searchSalesReps);
app.post('/employee/create', auth.authorize, EmployeeHandler.createEmployee);
app.get('/employee/:employeeId', auth.authorize, EmployeeHandler.getEmployeeByEmployeeId);
app.put('/employee/update/:employeeId', auth.authorize, EmployeeHandler.updateEmployee);
app.get('/employees/search', auth.authorize, EmployeeHandler.searchAllEmployees);
app.put('/employee/delete/:employeeIds', auth.authorize, EmployeeHandler.deleteEmployeesByIds);

/*TODO:
    app.get('/vehicle') - get ?q=toyota&index=1&size=10;
    app.post('/vehicle') - create
    app.put('/vehicle') - update
    app.delete('/vehicle/:id') - delete
    app.post('/vehicle/delete/') - delete multiple vehicles {id list should pass via body}
*/

app.post('/vehicle/create', auth.authorize, VehicleHandler.createVehicle);

app.get('/stock/search', auth.authorize, InventoryHandler.searchAvailableStock);

app.post('/item/return', auth.authorize, InventoryHandler.createReturnedItem);
app.get('/transaction/item/:itemId/type/:salesType', auth.authorize, InventoryHandler.searchAllSoldItems);


/* Error handling */
app.use((err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }

    Logger.log(err);

    if (err instanceof HttpException) {
        return res.status(err.code).send(err.message);
    }

    return res.status(500).send('Something broke!');
});

module.exports = app;


