'use strict';

(function (module) {
    module.controller('supplierCreateController', function ($scope, $routeParams, $location, $filter, $toaster, lookupService, sessionFactory, supplierService, viewbag) {

        var supplierId = null;
        var supplierCopy = null;
        $scope.showLoading = false;
        $scope.isEditing = false;
        $scope.errorMessage = null;
        $scope.showSaveButton = false;
        $scope.selectedRoute = null;
        $scope.categoryItems = [];
        $scope.supplier = new Entity.Supplier();

        function getSupplierDetails(supplierId) {
            if (viewbag.isExist('supplier')) {
                $scope.showSaveButton = true;
                $scope.supplier = viewbag.get('supplier');
                $scope.selectedLanguage.key = $scope.supplier.language;
                $scope.selectedLanguage.value = $scope.supplier.languageName;
                viewbag.remove('supplier');
                return;
            }
            $scope.showLoading = true;
            $scope.supplier = null;
            supplierService.getBySupplierById(supplierId).then(function (result) {
                if (result && result !== 'undefined') {
                    $scope.selectedLanguage.key = result.language;
                    $scope.selectedLanguage.value = result.languageName;
                    $scope.showSaveButton = true;
                    $scope.showLoading = false;
                    $scope.supplier = result;
                    supplierCopy = angular.copy(result);
                } else {
                    $scope.backToList();
                }
            }).catch(function (ex) {
                console.log(ex);
                $scope.showLoading = false;
            });
        }

        function getCategoryItems() {
            $scope.categoryItems = [];
            categoryService.getActiveCategories().then(function (results) {
                if (results) {
                    $scope.categoryItems = results;
                }
            }).catch(function (exception) {
                console.log(exception);
            });
        };

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        $scope.createNew = function () {
            if ($scope.isEditing) {
                $location.path('/supplier/create');
            } else {
                $route.reload();
            }
        };

        function fillLanguages() {
            $scope.languages = lookupService.getLookup('language');
            $scope.selectedLanguage = $scope.languages[0];
            $scope.supplier.language = $scope.languages[0].key;
            $scope.supplier.languageName = $scope.languages[0].value;
        }

        $scope.setSelectedLanguage = function () {
            $scope.supplier.language = $scope.selectedLanguage.key;
            $scope.supplier.languageName = $scope.selectedLanguage.value;
        };

        $scope.backToList = function () {
            $location.path("/supplier/list");
        }

        $scope.setSlectedCategory = function ($event, category) {
            $event.preventDefault();
            $scope.supplier.categoryId = category.id;
            $scope.supplier.category = category.category;
        };

        $scope.reset = function ($event) {
            if ($event) {
                $event.preventDefault();
            }

            $scope.showLoading = false;
            $scope.errorMessage = null;
            $scope.form.$setPristine();
            $scope.supplier = angular.copy(supplierCopy);
        }

        $scope.save = function (supplier) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid) {
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                supplier.isActive = 1;
                if (supplierId && $scope.supplier) {
                    supplierService.update(supplier).then(function (response) {
                        if (response.success) {
                            viewbag.put('supplier', supplier);
                            showSuccessToaster('Changes have been saved successfully');

                            $scope.form.$setPristine();
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                } else {
                    supplierService.create(supplier).then(function (response) {
                        if (response.success) {
                            supplier.supplierId = response.supplierId;

                            showSuccessToaster('Record has been successfully saved');
                            $scope.form.$setPristine();
                            $scope.supplier = new Entity.Supplier();
                        } else {
                            $scope.errorMessage = response.message;
                        }
                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                }
            }
        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                //getCategoryItems();
                fillLanguages();
                supplierId = $routeParams.supplierId ? decodeURIComponent($routeParams.supplierId) : null;
                if (supplierId) {
                    $scope.isEditing = true;
                    getSupplierDetails(supplierId);
                } else {
                    $scope.showSaveButton = true;
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.supplier'));