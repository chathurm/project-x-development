'use strict';

(function (module) {
    module.controller('supplierDetailsController', function ($scope, $route, $location, $routeParams, $timeout, $toaster, sessionFactory, supplierService, viewbag) {

        $scope.supplier = null;
        $scope.showLoading = false;
        $scope.showMap = true;

        function getSupplierDetails(id) {
            $scope.showLoading = true;
            supplierService.getBySupplierById(id).then(function (result) {
                if (result) {
                    $scope.supplier = result;
                    $scope.showLoading = false;
                } else {
                    $location.path("/supplier/list");
                }
            }).catch(function (ex) {
                console.log(ex);
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                $scope.showLoading = false;
            });
        };

        $scope.changeStatus = function (supplier) {
            $scope.showLoading = true;
            var isActive = supplier.isActive === 1 ? 0 : 1;
            supplierService.changeActiveStatus(supplier.id, isActive).then(function (result) {
                $scope.showLoading = false;
                $toaster.pop({
                    message: 'Successfully saved the new status',
                    type: 'success'
                });
                $route.reload();
            }).catch(function (ex) {
                $scope.showLoading = false;
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.updateDeleteStatus = function (supplier) {
            supplierService.changeDeleteStatus(supplier.id).then(function (result) {
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.editSupplier = function (supplier) {
            viewbag.put('supplier', supplier);
            $location.path("/supplier/edit/" + encodeURIComponent(supplier.id));
        };

        $scope.backToList = function () {
            $location.path("/supplier/list");
        }

        $scope.init = function () {
            var supplierId = decodeURIComponent($routeParams.supplierId);
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }
            if (viewbag.isExist('supplier')) {
                $scope.supplier = viewbag.get('supplier');
                viewbag.remove('supplier');
            } else {
                getSupplierDetails(supplierId);
            }
        };
    });
})(angular.module('project-x.supplier'));