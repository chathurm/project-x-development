(function (module) {
    module.service('paymentService', function (config, httpFactory) {

        this.create = function (payment) {
            var url = config.getBaseApi() + "payment/create";
            return httpFactory.post(url, payment).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getPaymentById = function (paymentId) {
            var url = config.getBaseApi() + "payment/" + encodeURIComponent(paymentId);
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getPendingPayments = function (customerId) {
            var url = config.getBaseApi() + "payment/pending/" + encodeURIComponent(customerId);
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + "payments/search";
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.payment'));