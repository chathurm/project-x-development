'use strict';

(function (module) {
    module.controller('paymentDetailsController', function ($scope, $window, $timeout, $route, $templateRequest, $compile, $toaster, $location, $routeParams, viewbag, sessionFactory, paymentService, permissionFactory) {

        var paymentId = null;
        $scope.showLoading = false;

        $scope.payment = new Entity.Payment();
        $scope.payment.items = [];

        function getPaymentDetails(paymentId) {
            if (viewbag.isExist('payment')) {
                $scope.payment = viewbag.get('payment');
                viewbag.remove('payment');
            } else {
                $scope.showLoading = true;
                paymentService.getPaymentById(paymentId).then(function (response) {
                    $scope.payment = response.paymentData ? response.paymentData[0] : null;
                    $scope.payment.items = response.paymentItems;
                    $scope.payment.cheques = response.paymentCheques;
                    $scope.showLoading = false;
                }).catch(function (err) {
                    $scope.showLoading = false;
                    console.log("Error in controller", err);
                });
            }
        }

        $scope.updateDeleteStatus = function (payment) {
            paymentService.changeDeleteStatus(payment.id).then(function (result) {
                $toaster.pop({
                    message: payment.code + ' Successfully Deleted',
                    type: 'success'
                });
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.print = function () {
            angular.element('#print').html('');
            $templateRequest("payment/payment.print.html").then(function (html) {
                angular.element('#print').append($compile(html)($scope));
                $timeout(function () {
                    $window.print();
                }, 200);
            });
        };

        $scope.backToList = function () {
            $location.path("/payment/list");
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                if (!permissionFactory.get().permissions.viewPayment) {
                    $location.path('/error/403');
                    return;
                }

                paymentId = $routeParams.paymentId ? $routeParams.paymentId : null;
                if (paymentId) {
                    getPaymentDetails(paymentId);
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

    });
})(angular.module('project-x.payment'));