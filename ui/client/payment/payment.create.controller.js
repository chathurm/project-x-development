'use strict';

(function (module) {
    module.controller('paymentCreateController', function ($scope, $route, $toaster, $window, $templateRequest, $timeout, $compile, $location, lookupService, paymentService, sessionFactory, permissionFactory) {

        $scope.showLoading = false;
        $scope.errorMessage = null;

        $scope.paymentTypes = [];

        $scope.payment = new Entity.Payment();
        $scope.cheque = new Entity.Cheque();
        $scope.payment.items = [];
        $scope.payment.cheques = [];

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        function calculateTotalPayment() {
            $scope.payment.amount = $scope.payment.items.reduce(function (prev, next) {
                return Number(prev) + Number(next.amount);
            }, 0);
        }

        function calculateChequeTotalAmount() {
            return $scope.payment.cheques.reduce(function (prev, next) {
                return Number(prev) + Number(next.amount);
            }, 0);
        }

        function fillPaymentTypes() {
            $scope.paymentTypes = lookupService.getLookup('paymentMethod');
            //remove the 'Credit' option from the dropdown
            $scope.paymentTypes.splice(1, 1);
            $scope.selectedPayType = $scope.paymentTypes[0];
            $scope.payment.paymentMethodId = $scope.paymentTypes[0].key;
            $scope.payment.paymentMethodName = $scope.paymentTypes[0].value;
        }

        function print() {
            angular.element('#print').html('');
            $templateRequest("payment/payment.print.html").then(function (html) {
                angular.element('#print').append($compile(html)($scope));
                $timeout(function () {
                    $window.print();
                    $route.reload();
                }, 200);
            });
        }

        function validatePayment(payment) {
            if (payment.items.length === 0) {
                return false;
            } else if (payment.amount === 0) {
                return false;
            } else if (payment.paymentMethodName === 'Cheque') {
                if (payment.cheques.length === 0) {
                    return false;
                } else if (calculateChequeTotalAmount() < $scope.payment.amount) {
                    return false;
                }
            } else {
                return true;
            }
        }

        function loadPendingPayments(customerId) {
            $scope.isLoadingInvoices = true;
            $scope.payment.items = [];
            paymentService.getPendingPayments(customerId).then(function (response) {
                $scope.payment.items = response;
                $scope.isLoadingInvoices = false;
            }).catch(function (err) {
                showErrorToaster('Something went wrong. Try again');
                $scope.isLoadingInvoices = false;
            });
        }

        $scope.setSelectedSalesType = function () {
            $scope.payment.salesTypeId = $scope.selectedSalesType.key;
            $scope.payment.salesTypeName = $scope.selectedSalesType.value;
        };

        $scope.setSelectedPayType = function () {
            $scope.payment.paymentMethodId = $scope.selectedPayType.key;
            $scope.payment.paymentMethodName = $scope.selectedPayType.value;
        };

        //cheques
        $scope.addCheque = function () {
            var cheque = $scope.cheque;
            if (cheque.chequeNo && cheque.bank && cheque.branch && cheque.amount) {
                var chequeTotal = calculateChequeTotalAmount() + Number(cheque.amount);
                if ($scope.payment.amount < chequeTotal) {
                    return;
                }
                $scope.payment.cheques.push(cheque);
                $scope.cheque = new Entity.Cheque();
            }
        };

        $scope.removeCheque = function ($event, index) {
            $event.preventDefault();
            $scope.payment.cheques.splice(index, 1);
        };

        //associated invoices
        $scope.removeItem = function ($event, index) {
            $event.preventDefault();
            $scope.payment.items.splice(index, 1);
            calculateTotalPayment();
        };

        $scope.validateAmount = function (invoice) {
            var element = angular.element('#item-' + invoice.id);
            var amount = element.val() ? Number(element.val()) : 0;
            if (invoice.dueAmount < amount) {
                invoice.amount = 0;
            }
            calculateTotalPayment();
        };

        $scope.saveChanges = function (payment, doPrint) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid || !validatePayment(payment)) {
                $scope.form.$setPristine();
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                payment.isActive = 1;

                paymentService.create(payment).then(function (response) {
                    if (response.success) {
                        $scope.payment.code = response.paymentCode;
                        $scope.payment.id = response.paymentId;

                        if (doPrint) {
                            print();
                        } else {
                            showSuccessToaster('Payment has been successfully saved');
                            $timeout(function () {
                                $route.reload();
                            }, 200);
                        }
                    } else {
                        $scope.errorMessage = response.message;
                    }
                    $scope.showLoading = false;
                }).catch(function (ex) {
                    showErrorToaster('Payment has not being saved. Try again');
                    $scope.showLoading = false;
                });
            }
        };

        $scope.customerSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.customerId, nic: o.nic };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    var template = '<div>'
                        + '<small class="helper-block">' + data.code + '</small>'
                        + '<p>'
                        + '<strong>' + data.text + '</strong><br/>'
                        + (data.nic ? ('<small>(' + data.nic + ')</small>') : '')
                        + '</p>'
                        + '</div>';

                    return $(template);
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                    $scope.payment.customerName = event.params.data.text;
                    loadPendingPayments(event.params.data.id);
                });
            }
        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                if (!permissionFactory.get().permissions.addPayment) {
                    $location.path('/error/403');
                    return;
                }

                fillPaymentTypes();
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.payment'));