(function (module) {
    module.service('dashboardService', function (httpFactory, config) {
        
        this.getOutOfStockItems = function(index, pageSize){
            index = !index ? 1 : index;
            pageSize = !pageSize ? 5 : pageSize;

            var url = config.getOutOfStockItemsUrl(index, pageSize);

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.dashboard'));