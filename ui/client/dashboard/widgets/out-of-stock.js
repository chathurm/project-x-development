(function (module) {
    module.directive('dashboardOutOfStock', function ($templateCache, sessionFactory, permissionFactory, dashboardService) {
        return {
            restrict:'E',
            replace:true,
            controller: function ($scope) {
                var pageSize = 5;
                var index = 1;
                var maximizeCount = 0;

                $scope.loading = true;
                $scope.items = [];
                $scope.total = 0;
                $scope.maximize = false;

                $scope.toggle = function($event){
                    $event.preventDefault();
                    $scope.maximize = !$scope.maximize;
                    maximizeCount += 1;

                    if($scope.maximize && maximizeCount === 1){
                        loadData(1, 100);
                    }
                };

                function loadData(index, limit) {
                    dashboardService.getOutOfStockItems(index, limit).then(function (data) {
                        if(data && data.records){
                            $scope.items = data.records;
                            $scope.total = data.recordCount;
                        }

                        $scope.loading = false;
                    }).catch(function (ex) {
                        reset();
                    });
                }

                function reset() {
                    $scope.items = [];
                    $scope.total = 0;
                    $scope.loading = false;
                }

                function init(){
                    if (sessionFactory.isAuthenticated()) {
                        loadData(index, pageSize);
                    }
                }

                init();

            },
            template: $templateCache.get('dashboard/widgets/out-of-stock.html') 
        };

    });
})(angular.module('project-x.dashboard'));