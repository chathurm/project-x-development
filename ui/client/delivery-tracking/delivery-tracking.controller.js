'use strict';

(function (module) {
    module.controller('deliveryTrackingController', function ($scope, $toaster, sessionFactory, itemIssueService, permissionFactory) {

        $scope.deliveredList = [];
        $scope.selectedDeliveredList = [];
        $scope.showNoResultMassage = false;
        $scope.deliveryStatus = 0;

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        $scope.getItemListByTackingStatus = function () {
            $scope.deliveredList.length = 0; //empty the list
            $scope.isLoadingItems = true;
            var orderTrackingStatus = parseInt($scope.deliveryStatus); //loaded
            if (orderTrackingStatus === 0) {
                getAllItemList();
            } 
            else {
                itemIssueService.getItemListByTackingStatus(orderTrackingStatus).then(function (response) {
                    $scope.deliveredList = response;
                    if ($scope.deliveredList.length === 0) {
                        $scope.showNoResultMassage = true;
                    } else {
                        $scope.deliveredList.forEach(function (item) {
                            item.isSelected = item.isSelected === 1;
                        });
                        $scope.showNoResultMassage = false;
                    }
                    $scope.isLoadingItems = false;
                }).catch(function (err) {
                    $scope.isLoadingItems = false;
                    console.log("Error in controller", err);
                });
            }
        };

        function getAllItemList() {
            $scope.deliveredList.length = 0; //empty the list
            $scope.isLoadingItems = true;
            itemIssueService.getAllItemList().then(function (response) {
                $scope.deliveredList = response;
                if ($scope.deliveredList.length === 0) {
                    $scope.showNoResultMassage = true;
                } else {
                    $scope.deliveredList.forEach(function (item) {
                        item.isSelected = item.isSelected === 1;
                    });
                    $scope.showNoResultMassage = false;
                }
                $scope.isLoadingItems = false;
            }).catch(function (err) {
                $scope.isLoadingItems = false;
                console.log("Error in controller", err);
            });
        };

        $scope.saveChanges = function () {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            $scope.selectedDeliveredList.length === 0;
            $scope.deliveredList.forEach(function (item) {
                if (item.isSelected) {
                    item.orderStatus = 3; //set order status to delivered
                    $scope.selectedDeliveredList.push(item);
                }
            });

            if ($scope.selectedDeliveredList.length === 0) {
                return;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                itemIssueService.updateItemTrackingStatus($scope.selectedDeliveredList).then(function (response) {
                    if (response.success) {
                        $scope.getItemListByTackingStatus();
                        showSuccessToaster('Record(s) have been successfully updated.');
                    } else {
                        $scope.errorMessage = response.message;
                    }
                    $scope.showLoading = false;
                }).catch(function (ex) {
                    showErrorToaster('Something went wrong. Please try again.');
                    $scope.showLoading = false;
                });
            }
        };

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.tracking'));