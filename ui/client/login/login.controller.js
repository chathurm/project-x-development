/* TODO https://security.googleblog.com/2016/09/moving-towards-more-secure-web.html */
(function (module) {
    module.controller('loginController', function ($scope, $rootScope, $location, loginService, sessionFactory, permissionFactory) {
        var storename = getStoreName();

        $scope.loading = false;
        $scope.loadingConfig = false;

        $scope.login = {
            username: null,
            password: null
        };

        $scope.user = angular.copy($scope.login);

        $scope.errorMessage = null;

        $scope.submit = function (user) {
            $scope.loading = true;
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (user && (!user.username || !user.password)) {
                hasValidationErrors = true;
                $scope.loading = false;
            }

            if (!hasValidationErrors) {
                $scope.login = angular.copy(user);
                loginService.login(storename, $scope.login.username, $scope.login.password).then(function (response) {
                    if (response.success) {

                        sessionFactory.createSession({ store: storename, username: user.username, token: response.token, created: new Date() });
                        $scope.loadingConfig = true;
                        permissionFactory.load(true).then(function () {
                            $scope.loadingConfig = false;
                            $rootScope.$broadcast('application:user:login:success');
                            $location.path('/');

                            var body = angular.element('body');
                            body.removeClass('login-page');

                        }).catch(function () {
                            $scope.loadingConfig = false;
                        });

                    } else {
                        $rootScope.$broadcast('application:user:login:fail');
                        $scope.errorMessage = response.message;
                    }

                    $scope.loading = false;

                }).catch(function (ex) {
                    console.log('error in controller', ex);
                });
            }
        };

        $scope.reset = function (form) {
            if (form) {
                form.$setPristine();
                form.$setUntouched();
            }

            $scope.loading = false;
            $scope.loadingConfig = false;
            $scope.user = angular.copy($scope.login);
            $scope.errorMessage = null;
        };

        $scope.init = function () {
            var body = angular.element('body');
            if (!body.hasClass('login-page')) {
                body.addClass('login-page');
            }

            if (sessionFactory.isAuthenticated()) {
                body.removeClass('login-page');
                $location.url('/');
            }
        }

        function getStoreName() {
            return angular.element('#store').val();
        }
    });
})(angular.module('project-x.login'));