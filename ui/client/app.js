'use strict';

angular.module('project-x', ['ngRoute', 'ngSanitize', 'project-x.modules-all', 'project-x.utils', 'project-x.templates']);

(function (module) {
    module.config(function ($routeProvider, $compileProvider) {
        //$compileProvider.debugInfoEnabled(false);

        var permissionResolver = {
            userClaim: function ($q, $rootScope, sessionFactory, permissionFactory) {
                if (!sessionFactory.isAuthenticated()) {
                    sessionFactory.redirectToLoginPage();
                    return;
                }

                var deferred = $q.defer();
                permissionFactory.load().then(function () {
                    deferred.resolve(true);
                }).catch(function (exception) {
                    if (exception && exception.status) {
                        switch (exception.status) {
                            case 401:
                            case 403:
                                $rootScope.$broadcast('application:force-logout');
                                break;
                        }
                    }
                });

                return deferred.promise;
            }
        };

        /*http://odetocode.com/blogs/scott/archive/2014/05/20/using-resolve-in-angularjs-routes.aspx*/

        $routeProvider
            .when('/', {
                templateUrl: 'dashboard/dashboard.html',
                controller: 'dashboardController',
                resolve: permissionResolver
            })
            .when('/guide', {
                templateUrl: 'guide/guide.html',
                controller: 'guideController',
                resolve: permissionResolver
            })
            .when('/error/403', {
                templateUrl: 'errors/403.html'
            })
            .when('/error/failure', {
                templateUrl: 'errors/failure.html'
            })
            .when('/login', {
                templateUrl: 'login/login.html'
            })
            .when('/customer/create', {
                templateUrl: 'customer/create/create.html'
            })
            .when('/customer/edit/:customerId', {
                templateUrl: 'customer/create/create.html'
            })
            .when('/customer/details/:customerId', {
                templateUrl: 'customer/details/details.html'
            })
            .when('/customer/list', {
                templateUrl: 'customer/list/list.html',
                controller: 'customerListController',
                resolve: permissionResolver
            })
            .when('/supplier/create', {
                templateUrl: 'supplier/supplier.create.html'
            })
            .when('/supplier/edit/:supplierId', {
                templateUrl: 'supplier/supplier.create.html'
            })
            .when('/supplier/details/:supplierId', {
                templateUrl: 'supplier/supplier.details.html'
            })
            .when('/supplier/list', {
                templateUrl: 'supplier/supplier.list.html'
            })
            .when('/line/list', {
                templateUrl: 'line/list/list.html',
                controller: 'lineListController',
                resolve: permissionResolver
            })
            .when('/line/create', {
                templateUrl: 'line/create/create.html',
                controller: 'lineCreateController',
                resolve: permissionResolver
            })
            .when('/line/edit/:lineId', {
                templateUrl: 'line/create/create.html',
                controller: 'lineCreateController',
                resolve: permissionResolver
            })
            .when('/line/details/:lineId', {
                templateUrl: 'line/details/details.html',
                controller: 'lineDetailsController',
                resolve: permissionResolver
            })
            .when('/item/list', {
                templateUrl: 'item/item.list.html',
                resolve: permissionResolver
            })
            .when('/item/create', {
                templateUrl: 'item/item.create.html',
                controller: 'itemCreateController',
                resolve: permissionResolver
            })
            .when('/item/edit/:itemId', {
                templateUrl: 'item/item.create.html',
                controller: 'itemCreateController',
                resolve: permissionResolver
            })
            .when('/item/details/:itemId', {
                templateUrl: 'item/item.details.html',
                controller: 'itemDetailsController',
                resolve: permissionResolver
            })
            .when('/inventory/create', {
                templateUrl: 'inventory/create/create.html',
                controller: 'inventoryCreateController',
                resolve: permissionResolver
            })
            .when('/inventory/edit/:inventoryId', {
                templateUrl: 'inventory/create/create.html',
                controller: 'inventoryCreateController',
                resolve: permissionResolver
            })
            .when('/inventory/list', {
                templateUrl: 'inventory/list/list.html',
                controller: 'inventoryListController',
                resolve: permissionResolver
            })
            .when('/inventory/details/:inventoryId', {
                templateUrl: 'inventory/details/details.html',
                controller: 'inventoryDetailsController',
                resolve: permissionResolver
            })
            .when('/invoice/create', {
                templateUrl: 'invoice/invoice.create.html',
                controller: 'invoiceCreateController',
                resolve: permissionResolver
            })
            .when('/invoice-ng/create', {
                templateUrl: 'invoice/invoice.create-ng.html',
                controller: 'invoiceCreateNgController',
                resolve: permissionResolver
            })
            .when('/invoice/details/:invoiceId', {
                templateUrl: 'invoice/invoice.details.html',
                controller: 'invoiceDetailsController',
                resolve: permissionResolver
            })
            .when('/invoice/list', {
                templateUrl: 'invoice/invoice.list.html',
                controller: 'invoiceListController',
                resolve: permissionResolver
            })
            .when('/invoice/due/list', {
                templateUrl: 'invoice/invoice.due.list.html',
                controller: 'invoiceDueListController',
                resolve: permissionResolver
            })
            .when('/grn/create', {
                templateUrl: 'grn/create.html',
                controller: 'grnCreateController',
                resolve: permissionResolver
            })
            .when('/grn/details/:grnId', {
                templateUrl: 'grn/create.html',
                controller: 'grnCreateController',
                resolve: permissionResolver
            })
            .when('/grn/list', {
                templateUrl: 'grn/list.html',
                controller: 'grnListController',
                resolve: permissionResolver
            })
            .when('/cms/lookups', {
                templateUrl: 'cms/lookup.html',
                controller: 'lookupController',
                resolve: permissionResolver
            })
            .when('/product/create', {
                templateUrl: 'product/product.create.html'
            })
            .when('/product/details/:productId', {
                templateUrl: 'product/product.details.html'
            })
            .when('/product/list', {
                templateUrl: 'product/product.list.html'
            })
            .when('/custom-product/create', {
                templateUrl: 'custom-product/custom-product.create.html'
            })
            .when('/custom-product/details/:customerProductId', {
                templateUrl: 'custom-product/custom-product.details.html'
            })
            .when('/custom-product/list', {
                templateUrl: 'custom-product/custom-product.list.html'
            })

            .when('/material/create', {
                templateUrl: 'product/material.create.html'
            })

            .when('/employee/create', {
                templateUrl: 'employee/create.html'
            })
            .when('/employee/edit/:employeeId', {
                templateUrl: 'employee/create.html'
            })
            .when('/employee/list', {
                templateUrl: 'employee/list.html'
            })
            .when('/item-issue/create', {
                templateUrl: 'inventory-issue/item-issue.create.html'
            })
            .when('/item-issue/details/:itemIssueId', {
                templateUrl: 'inventory-issue/item-issue.details.html'
            })
            .when('/item-issue/list', {
                templateUrl: 'inventory-issue/item-issue.list.html'
            })
            .when('/item-issue/loading-sheet', {
                templateUrl: 'inventory-issue/item-issue.loading.html'
            })
            .when('/payment/create', {
                templateUrl: 'payment/payment.create.html'
            })
            .when('/payment/details/:paymentId', {
                templateUrl: 'payment/payment.details.html'
            })
            .when('/payment/list', {
                templateUrl: 'payment/payment.list.html'
            })
            .when('/order-taking/list', {
                templateUrl: 'order-taking/order-taking-list.html',
                controller: 'orderTakingListController',
                resolve: permissionResolver
            })
            .when('/order-taking/create/:customerId', {
                templateUrl: 'order-taking/order-taking-create.html',
                controller: 'orderTakingCreateController',
                resolve: permissionResolver
            })
            .when('/vehicle/create', {
                templateUrl: 'vehicle/create/create.html',
                controller: 'vehicleCreateController',
                resolve: permissionResolver
            })
            .when('/item/return', {
                templateUrl: 'item/item.return.html',
                controller: 'itemReturnController',
                resolve: permissionResolver
            })
            .when('/delivery/tracking', {
                templateUrl: 'delivery-tracking/delivery-tracking.html',
                controller: 'deliveryTrackingController'
            })
            .when('/available-stock/list', {
                templateUrl: 'stock/list.html',
                controller: 'grnListController',
                resolve: permissionResolver
            })
            .when('/item-stock-log/:itemId', {
                templateUrl: 'item-stock-log/stock-log.html',
                controller: 'itemStockLogController',
                resolve: permissionResolver
            })
            .otherwise({
                redirectTo: '/'
            });
    })
        .run(function () {

        })
        .controller('rootController', function ($scope, $rootScope, sessionFactory) {
            /*$rootScope.$on('$routeChangeStart', function($event,next,current){
                console.log('current', current);
                console.log('next', next);
            });*/
        })
})(angular.module('project-x'));