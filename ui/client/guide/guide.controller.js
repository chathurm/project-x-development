(function (module) {
    module.controller('guideController', function ($scope, sessionFactory, permissionFactory) {
        $scope.fruit = null;
        $scope.customer = null;

        $scope.init = function () {

        };

        $scope.config = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1; 

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template : function(data, element){
                /*
                    when loading data you will get following object
                    data = { disabled : true, loading : true, text: 'Searching...' }
                */
                if(!data.loading){
                    var t = data.text.split(',');
                    return $('<i class="glyphicon glyphicon-fire"></i> <span>' + t[0] + '</span> - \t' + t[1]); 
                }

                return 'Please wait..';
            },
            onSelect : function(event){
                console.log(event.params.data);
            }
        };

        $scope.copySelected = function(){
            alert('Copped to clipboard');
        };
    });
})(angular.module('project-x.dashboard'));