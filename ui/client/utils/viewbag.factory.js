(function (module) {
    module.factory('viewbag', function () {
        var viewbag = {};

        function put(key, value) {
            viewbag[key] = value;
        }
        function get(key) {
            return viewbag[key] ? viewbag[key] : null;
        }
        function remove(key) {
            delete viewbag[key];
        }
        function removeAll() {
            viewbag = {};
        }
        function isExist(key) {
            return viewbag[key] !== undefined;
        }
        return {
            get: get,
            put: put,
            remove: remove,
            isExist: isExist,
            removeAll: removeAll
        };
    });

})(angular.module('project-x.utils'));