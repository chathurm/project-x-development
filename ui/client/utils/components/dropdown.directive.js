(function (module) {
    module.directive('select', function (sessionFactory) {
        return {
            restrict: 'E',
            scope: {
                config: '='
            },
            link: function ($scope, $element, $attr) {
                var ajaxUrl = $attr.ajaxDataUrl;
                var searchable = true;

                var displaySize = 10;

                var config = {};

                if ($attr.searchable) {
                    searchable = $attr.searchable == 'true';
                }

                if (ajaxUrl) {
                    config.ajax = {
                        data: function (params) {
                            return {
                                searchText: params.term || 'null',
                                index: params.page || 1,
                                size: displaySize
                            };
                        },
                        delay: 250,
                        url: ajaxUrl,
                        maximumInputLength: 5,
                        minimumInputLength: 1,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('x-access-token', sessionFactory.getSession().token);
                        },
                        cache: $attr.ajaxDataCache == 'true'
                    };

                    if ($scope.config) {
                        if ($scope.config.processResults && angular.isFunction($scope.config.processResults)) {
                            config.ajax.processResults = function (data, params) {
                                return $scope.config.processResults(data, params, displaySize);
                            }
                        }

                        if ($scope.config.searchable) {
                            searchable = $scope.config.searchable;
                        }

                        if($scope.config.initialData){
                            var defaultOption = $scope.config.initialData;
                            setTimeout(function(){
                                var $option = $('<option selected></option>').text(defaultOption.text).val(defaultOption.id);
                                $element.append($option).trigger('change');
                            }, 500);
                        }
                    }

                    if (!$scope.config || !$scope.config.processResults) {
                        config.ajax.processResults = defaultProcessResult
                    }
                }

                if ($scope.config && ($scope.config.template && angular.isFunction($scope.config.template))) {
                    config.templateResult = $scope.config.template;
                }

                if (!searchable) {
                    config.minimumResultsForSearch = Infinity;
                }

                $element.select2(config);

                if ($scope.config && ($scope.config.onSelect && angular.isFunction($scope.config.onSelect))) {
                    $element.on("select2:select", $scope.config.onSelect);
                }

                function defaultProcessResult(data, params) {
                    return {
                        results: data.records || [],
                        pagination: {
                            more: ((params.page || 1) * displaySize) < data.recordCount
                        }
                    };
                }
            }
        };
    });

})(angular.module('project-x.utils'));