(function (module) {
    module.directive('format', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;

                ctrl.$formatters.unshift(function (a) {
                    if (attrs.format === 'currency') {
                        return $filter('currency')(ctrl.$modelValue, '');
                    } else {
                        return $filter(attrs.format)(ctrl.$modelValue);
                    }
                });

                elem.bind('blur', function (event) {
                    var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
                    if (attrs.format === 'currency') {
                        elem.val($filter(attrs.format)(plainNumber, ''));
                    } else {
                        elem.val($filter(attrs.format)(plainNumber));
                    }
                });
            }
        };
    });
})(angular.module('project-x.utils'));