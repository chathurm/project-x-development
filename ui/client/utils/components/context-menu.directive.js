(function (module) {
    module.directive('contextMenu', function (sessionFactory) {
        return {
            restrict: 'A',
            scope:{
                context:'@',
                autoClose:'@'
            },
            link: function ($scope, $element, $attr) {
                $element.addClass('context').attr('hidden','');

                angular.element($scope.context).bind("contextmenu", function (event) {
                    event.preventDefault();
                    $(".context")
                        .show()
                        .css({
                            top: event.pageY + 15,
                            left: event.pageX + 10
                        });
                });

                if($scope.autoClose){
                    $element.find('li:not(.seperator)').on('click', function(){
                        $(".context").fadeOut("fast");
                    });
                }

                angular.element(document).click(function () {
                    var isHovered = $(".context").is(":hover");
                    if (isHovered == true) {
                        //nothing
                    } else {
                        $(".context").fadeOut("fast");
                    }
                });
            }
        };
    });

})(angular.module('project-x.utils'));