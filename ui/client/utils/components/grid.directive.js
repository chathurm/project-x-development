(function (module) {
    module.directive('grid', function ($templateCache, $routeParams, $location, config) {
        return {
            restrict: 'E',
            scope: {
                adapter: '='
            },
            transclude: true,
            link: function ($scope, $element, $attr, control, transclude) {
                var pageIndex = 1;

                $scope.loading = false;
                $scope.data = null;
                $scope.recentData = null;
                $scope.columns = getColumnsToDisplay();
                $scope.selectable = $attr.selectable == "true";
                $scope.enableEdit = $attr.enableEdit == "true";
                $scope.enableDelete = $attr.enableDelete == "true";
                $scope.maxRecordCount = config.getPageSize();
                $scope.keyword = null;
                $scope.allRowsSelected = false;
                $scope.selectCount = 0;
                $scope.actions = getActionBarActions();
                $scope.showRecent = $attr.enableRecent == "true";
                $scope.colspan = $scope.columns.length + ($scope.selectable ? 1 : 0) + (($scope.enableEdit || $scope.enableDelete) ? 1 : 0);
                
                function loadGrid(resetIndex) {
                    if($scope.adapter){
                        if (angular.isFunction($scope.adapter.dataSource)) {
                            $scope.loading = true;
                            $scope.adapter.dataSource($scope.keyword, pageIndex, $scope.adapter.queryParams).then(function (data) {
                                $scope.data = data;

                                $scope.loading = false;
                                selectAllRecords(false);

                            }).catch(function (ex) {
                                $scope.loading = false;
                                console.log('Error loading ', ex);
                            });
                        }

                        if ($scope.showRecent && angular.isFunction($scope.adapter.recentDataSource)) {
                            $scope.adapter.recentDataSource(5).then(function (data) {
                                if(data && data.records && data.records.length > 0){
                                    $scope.recentData = data;
                                }else{
                                    $scope.showRecent = false;
                                    $scope.recentData = null;
                                }
                            }).catch(function (ex) {
                                console.log('Error loading ', ex);
                            });
                        }
                    }
                }

                function getColumnsToDisplay() {
                    var columns = [];
                    var config = transclude();
                    angular.forEach(config, function (object, k) {
                        if ($(object).is('columns')) {
                            $(object).children().each(function (e) {
                                columns.push({
                                    name: $(this).prop("tagName"),
                                    header: $(this).attr("header"),
                                    align: $(this).attr("align"),
                                    format: $(this).attr("format")
                                });
                            });
                        }
                    });

                    return columns;
                }

                function getActionBarActions() {
                    var actions = [];
                    var config = transclude();
                    angular.forEach(config, function (object, k) {
                        if ($(object).is('actions')) {
                            $(object).children().each(function (e) {
                                actions.push({
                                    text: $(this).attr("text"),
                                    command: $(this).attr("command")
                                });
                            });
                        }
                    });

                    return actions;
                }

                function selectAllRecords(state) {
                    $scope.selectCount = 0;
                    if ($scope.data && $scope.data.records) {
                        angular.forEach($scope.data.records, function (r) {
                            r.selected = state;
                            $scope.selectCount = $scope.selectCount + (state ? 1 : 0);
                        });
                    }
                }

                function displayFormat(val, formatType) {
                    if (val) {
                        var v = null;
                        switch (formatType) {
                            case 'int':
                                return parseInt(val);

                            case 'float':
                                return parseFloat(val).toFixed(2);

                            case 'currency':
                                return 'Rs.' + parseFloat(val).toFixed(2);

                            case 'date':
                                return formatDate(new Date(val));

                            default:
                                return val;

                        }
                    }

                    return val;
                }

                function formatDate(date) {
                    var monthNames = [
                        "January", "February", "March",
                        "April", "May", "June", "July",
                        "August", "September", "October",
                        "November", "December"
                    ];

                    var day = date.getDate();
                    var monthIndex = date.getMonth();
                    var year = date.getFullYear();

                    return day + ' ' + monthNames[monthIndex] + ' ' + year;
                }

                function init() {
                    if ($routeParams.keyword || $routeParams.keyword !='') {
                        $scope.keyword = $routeParams.keyword;
                    }

                    if ($routeParams.page) {
                        if ($routeParams.page < 1) {
                            $location.search('page', 1);
                        }

                        pageIndex = parseInt($routeParams.page);
                    }

                    if ($scope.adapter) {
                        $scope.adapter.reload = loadGrid;
                    }

                    if($scope.showRecent){
                        $scope.showRecent = (pageIndex === 1 && !$scope.keyword);
                    }

                    $element.find('.dropdown-toggle').dropdown();
                }

                $scope.getColumnValue = function (object, prop, format) {
                    for (var p in object) {
                        if (p.toUpperCase() == prop) {
                            return object[p] ? displayFormat(object[p], format)  : "-";
                        }
                    }

                    return null;
                }

                $scope.search = function () {
                    pageIndex = 1;
                    $location.search('page', 1);
                    $location.search('keyword', $scope.keyword);

                    loadGrid(true);
                };

                $scope.getColumnHeader = function (column) {
                    return column.header || column.name;
                };

                $scope.selectAll = function ($event, state) {
                    selectAllRecords(state);
                };

                $scope.selectRecord = function ($event, state) {
                    $scope.selectCount += (state ? 1 : -1);

                    if ($scope.selectCount < 0) {
                        $scope.selectCount = 0;
                    }

                    if ($scope.selectCount == 0) {
                        $scope.allRowsSelected = false;
                    } else if ($scope.selectCount == $scope.maxRecordCount) {
                        $scope.allRowsSelected = true;
                    }
                };

                $scope.onActionClick = function ($event, command) {
                    if ($scope.adapter.onActionCommand) {
                        var selectedRows = $scope.data.records.filter(function (record) {
                            return record.selected;
                        });

                        $event.command = command;

                        $scope.adapter.onActionCommand($event, selectedRows);
                    }
                };

                $scope.onRowClick = function ($event, record, command) {
                    if ($scope.adapter.onRowCommand) {
                        $event.command = command;
                        $scope.adapter.onRowCommand($event, record);
                    }
                };

                $scope.$watch('adapter', function (newValue, oldValue, scope) {
                    loadGrid(true);
                });

                init();
            },
            template: $templateCache.get('utils/components/grid.html')
        };
    });

})(angular.module('project-x.utils'));