(function(module) {
    module.directive('siderbar', function($templateCache){
        return {
            restrict : 'E',
            replace : true,
            controller : function($scope, $timeout, $location, sessionFactory, permissionFactory){
                $scope.visible = sessionFactory.isAuthenticated();

                $scope.openLink = function($event, link){
                    $location.url(link);
                };

                /* events */
                $scope.$on('application:user:login:success', function(){
                    $scope.visible = true;
                });
            },
            link : function($scope, $element){
            },
            template : $templateCache.get('utils/siderbar.html')
        };
    });

})(angular.module('project-x.utils'));