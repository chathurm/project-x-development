(function (module) {
    module.directive('ajustLoading', function ($window) {
        return {
            link: function (scope, element) {
                element.css("min-height", $window.innerHeight - 200 + "px");
            }
        }
    });

})(angular.module('project-x.utils'));