(function (module) {
    module.service('$toaster', function ($rootScope) {
        this.pop = function (notification) {
            if (!notification.html) {
                notification.html = false;
            }

            this.toast = {
                title: notification.title,
                message: notification.message,
                type: notification.type,
                html: notification.html
            };

            $rootScope.$broadcast('toaster-newToast');
        };

        this.success = function(message){
            this.pop({ message: message, type: 'success' });
        };

        this.warning = function(message){
            this.pop({ message: message, type: 'warning' });
        };

        this.info = function(message){
            this.pop({ message: message, type: 'info' });
        };

        this.error = function(message){
            this.pop({ message: message, type: 'error' });
        };
    })
        .directive('toaster', function ($toaster, $timeout, $templateCache) {
            var controller = function ($scope, $element) {
                var timerId;

                $scope.notification = null;
                $scope.visible = false;

                function addNotification() {
                    stopOldTimer();
                    $scope.notification = {
                        title: $toaster.toast.title,
                        message: $toaster.toast.message,
                        type: $toaster.toast.type,
                        html: $toaster.toast.html
                    };
                    show();
                    startNewTimer();
                }

                function stopOldTimer() {
                    if (timerId !== undefined) {
                        $timeout.cancel(timerId);
                    }
                }

                function startNewTimer() {
                    timerId = $timeout(function () {
                        hide();
                        timerId = undefined;
                    }, 6000);
                }

                function show() {
                    $element.removeClass('opening').addClass('opening');
                    $scope.visible = true;
                }

                function hide() {
                    $element.removeClass('opening');
                    $scope.visible = false;
                }

                $scope.getIcon = function () {
                    if ($scope.notification) {
                        switch ($scope.notification.type) {
                            case 'error':
                                return 'fa-exclamation-triangle';
                            case 'warning':
                                return 'fa-exclamation-circle';
                            case 'success':
                                return 'fa-check';
                            case 'info':
                                return 'fa-info-circle';
                            default:
                                return null;
                        }
                    }

                    return null;
                };

                $scope.closeNotification = function () {
                    hide();
                    stopOldTimer();
                };

                $scope.$on('toaster-newToast', function () {
                    addNotification();
                });
            };


            return {
                restrict: 'E',
                controller: controller,
                scope: true,
                replace: true,
                template: $templateCache.get('utils/toaster.html')
            };
        });
})(angular.module('project-x.utils'));