(function (module) {
    module.service('config', function (cookieService, $window, $location) {

        var config = function () {
            return serviceVars;
        };

        var baseApi = function () {
            return config().application.api + '/';
        };

        this.fileUploadUrl = function () {
            return config().application.uploadUrl;
        };

        this.getBaseApi = function () {
            return baseApi();
        };

        this.getPageSize = function () {
            return 20; // TODO: read from config
        };

        //*******************Dashboard************************/

        this.getOutOfStockItemsUrl = function (index, pageSize) {
            return baseApi() + 'dashboard/outofstock-items?index=' + index + '&size=' + pageSize;
        };

        //*******************Customer************************/

        this.getCustomerCreateUrl = function () {
            return baseApi() + 'customer/create';
        };

        this.getCustomerByIdUrl = function (customerId) {
            return baseApi() + 'customer/' + encodeURIComponent(customerId);
        };

        this.getCustomerUpdateUrl = function (customerId) {
            return baseApi() + 'customer/update/' + encodeURIComponent(customerId);
        };

        this.getCustomerDeactivateUrl = function (customerId) {
            return baseApi() + 'customer/deactivate/' + encodeURIComponent(customerId);
        };

        this.getCustomerDeleteUrl = function (customerId) {
            return baseApi() + 'customer/delete/' + encodeURIComponent(customerId);
        };

        this.getCustomerSearchUrl = function (searchBy) {
            return baseApi() + 'customers/search';
        };

        //*******************Line************************/

        this.getLineSearchUrl = function () {
            return baseApi() + 'lines/search';
        }

        this.lineCreateUrl = function () {
            return baseApi() + 'line/create';
        }

        this.getActiveLines = function () {
            return baseApi() + 'lines/active';
        }

        this.getLineByIdUrl = function (lineId) {
            return baseApi() + 'line/' + encodeURIComponent(lineId);
        }

        this.getLineUpdateUrl = function (lineId) {
            return baseApi() + 'line/update/' + encodeURIComponent(lineId);
        };

        this.getLineDeleteUrl = function () {
            return baseApi() + 'line/delete';
        }
        //*******************Inventory************************/

        this.getItemCreateUrl = function () {
            return baseApi() + 'item/create';
        };

        this.getItemByIdUrl = function (itemId) {
            return baseApi() + 'item/' + encodeURIComponent(itemId);
        };

        this.getAllItemsUrl = function () {
            return baseApi() + 'items/active';
        };

        this.getSearchItemUrl = function (keyword) {
            return baseApi() + 'item/items' + encodeURIComponent(keyword);
        };

        this.getSearchGRNUrl = function () {
            return baseApi() + 'grn/search';
        };

        this.getUpdateItemUrl = function (item) {
            return baseApi() + 'item/update' + encodeURIComponent(item);
        };

        this.getDeleteItemUrl = function (itemId) {
            return baseApi() + 'item/delete' + encodeURIComponent(itemId);
        };

        this.inventoryItemCreateUrl = function () {
            return baseApi() + 'inventory/create';
        };

        this.grnCreateUrl = function () {
            return baseApi() + 'grn/create';
        };

        this.getinventoryItemByIdUrl = function (inventoryId) {
            return baseApi() + 'inventory/' + encodeURIComponent(inventoryId);
        };

        this.getAllItemsInTheInventoryUrl = function () {
            return baseApi() + 'inventory/view/all/current';
        };
        this.getGRNDetailsByIdUrl = function (grnId) {
            return baseApi() + 'grn/' + encodeURIComponent(grnId);
        };

        this.getUpdateInventoryItemUrl = function (inventoryId) {
            return baseApi() + '/inventory/update/' + encodeURIComponent(inventoryId);
        };

        this.getUpdateGRNUrl = function (grnId) {
            return baseApi() + '/grn/update/' + encodeURIComponent(grnId);
        };

        this.getInventorySearchUrl = function () {
            return baseApi() + 'inventories/search';
        };

        //********************End of Inventory****************/

        this.getAllSuppliersUrl = function () {
            return baseApi() + 'suppliers/active';
        };

        //********************  Lookups   ****************/

        this.getLookupCategoriesUrl = function () {
            return baseApi() + 'lookups/category';
        };

        this.createLookupCategoryUrl = function () {
            return baseApi() + 'lookups/category';
        };

        this.getLookupsUrl = function (categoryId) {
            return baseApi() + 'lookups/category/' + categoryId;
        };

        this.getCreateLookupUrl = function () {
            return baseApi() + 'lookups';
        };

        //************ Customer Products ************ */

        this.getCustomreProductForCustomerUrl = function (customerId) {
            return baseApi() + 'customer-product/customer/' + customerId;
        };

        this.getProductItemUrl = function (productId) {
            return baseApi() + 'product/' + productId;
        };

        this.getCompositeItemDeliveryStatusUrl = function(productId){
            return baseApi() + '/product/'+ productId +'/composite-items/delivery-status';
        };

        this.getCustomProductItemsSaveForTermsUrl = function (productId) {
            return baseApi() + 'customer-product/' + productId + '/terms/items';
        };

        this.getCustomerProductItemsForProductUrl = function (productId) {
            return baseApi() + 'customer-product/items/' + productId;
        };

    });

})(angular.module('project-x.utils'));
