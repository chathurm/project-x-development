(function (module) {
    module.directive('btnLoading', function () {
        return {
            restrict: 'A',
            scope: {
                btnLoading: '='
            },
            link: function ($scope, $element, $attrib) {
                var loadingTemplate = $attrib.btnLoadingSpinner == 'true' ? '<i class="fa fa-circle-o-notch fa-spin\"></i> ' + $attrib.btnLoadingText : $attrib.btnLoadingText;
                $element.attr('data-loading-text', loadingTemplate);

                $scope.$watch('btnLoading', function (loading) {
                    $element.button(loading ? 'loading' : 'reset');
                }, true);
            }
        }
    });

})(angular.module('project-x.utils'));