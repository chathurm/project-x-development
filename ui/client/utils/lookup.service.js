(function (module) {
    module.service('lookupService', function ($q, $location) {

        // this.loadLookups = function (storeName) {
        //     var deffered = $q.defer();

        //     $.getScript('/api/' + storeName + '/lookups.json').done(function () {
        //         deffered.resolve();
        //     }).fail(function (jqxhr, settings, exception) {
        //         deffered.reject(exception);
        //     });

        //     return deffered.promise;
        // };

        this.getLookup = function (category) {
            if (lookups === undefined) {
                $location.url('/error/failure');
            } else {
                var items = lookups.filter(function (obj) {
                    return obj.code === category;
                });
                return items;
            }
        };
    });
})(angular.module('project-x.utils'));