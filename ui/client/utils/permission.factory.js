(function (module) {
    module.factory('permissionFactory', function ($q, httpFactory, config, sessionFactory) {
        var userClaim = null;

        function loadUserClaim(reload) {
            var deffered = $q.defer();

            if (reload) {
                userClaim = null;
            }

            if (sessionFactory.isAuthenticated()) {
                if (userClaim) {
                    deffered.resolve();
                } else {

                    var url = config.getBaseApi() + "user/permission";
                    httpFactory.get(url)
                        .then(function (response) {
                            mapClaim(response.data);
                            deffered.resolve();
                        }).catch(function (exception) {
                            deffered.reject(exception);
                        });
                }
            } else {
                deffered.reject();
            }

            return deffered.promise;
        }

        function get() {
            return userClaim;
        }

        function expireCache() {
            userClaim = null;
        }

        function mapClaim(claim) {
            userClaim = {};
            userClaim.role = claim.role;
            userClaim.permissions = {};
            userClaim.permissions.customerSearch = hasPermission(claim.permissions, 'CUSTOMERS_SEARCH');
            userClaim.permissions.customerAdd = hasPermission(claim.permissions, 'CUSTOMERS_ADD');
            userClaim.permissions.inventoryAdd = hasPermission(claim.permissions, 'INVENTORY_ADD');
            userClaim.permissions.inventoryEdit = hasPermission(claim.permissions, 'INVENTORY_EDIT');
            userClaim.permissions.createLine = hasPermission(claim.permissions, 'LINE_ADD');
            userClaim.permissions.viewLine = hasPermission(claim.permissions, 'LINE_VIEW');
            userClaim.permissions.editLine = hasPermission(claim.permissions, 'LINE_EDIT');
            userClaim.permissions.deleteLine = hasPermission(claim.permissions, 'LINE_DELETE');
            userClaim.permissions.addGRN = hasPermission(claim.permissions, 'GRN_ADD');
            userClaim.permissions.viewGRN = hasPermission(claim.permissions, 'GRN_VIEW_DETAILS');
            userClaim.permissions.addInvoice = hasPermission(claim.permissions, 'INVOICE_ADD');
            userClaim.permissions.searchInvoices = hasPermission(claim.permissions, 'INVOICE_SEARCH');
            userClaim.permissions.viewInvoice = hasPermission(claim.permissions, 'INVOICE_VIEW_DETAILS');
            userClaim.permissions.addPayment = hasPermission(claim.permissions, 'PAYMENT_ADD');
            userClaim.permissions.viewPayment = hasPermission(claim.permissions, 'PAYMENT_VIEW_DETAILS');
        }

        function hasPermission(permissions, code) {
            return permissions.indexOf(code) > -1;
        }

        return {
            load: loadUserClaim,
            get: get,
            expireCache: expireCache
        };
    });

})(angular.module('project-x.utils'));