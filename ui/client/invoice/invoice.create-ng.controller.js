'use strict';

(function (module) {
    module.controller('invoiceCreateNgController', function ($scope, inventoryService) {
        $scope.keyword = null;
        $scope.isItemLoading = false;
        $scope.inventoryItems = [];

        $scope.search = function(keyword){
            $scope.inventoryItems = [];
            $scope.isItemLoading = true;

            inventoryService.searchInventoryItems(keyword, 0, 100).then(function(response){
                if(response){
                    $scope.inventoryItems = response.records;
                }
                $scope.isItemLoading = false;
            }).catch(function(exception){
                console.log(exception);
                $scope.isItemLoading = false;
            });
        };

        $scope.emptySearchResults = function(keyword) {
            if(!keyword){
                $scope.inventoryItems = [];
            }
        };

        $scope.addItemToInvoice = function(item){
            $scope.inventoryItems = [];
            $scope.keyword = null;
            console.log(item);
        };

        $scope.init = function(){

        };
    });
})(angular.module('project-x.invoice'));