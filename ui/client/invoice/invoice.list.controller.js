'use strict';

(function (module) {
    module.controller('invoiceListController', function ($scope, $location, sessionFactory, invoiceService, viewbag, config, permissionFactory) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteInvoices(records);
                    break;
            }
        }

        function onRowClick($event, invoice) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path("/invoice/details/" + encodeURIComponent(invoice.id));
                    break;
                case 'delete':
                    deleteInvoices([invoice]);
                    break;
            }
        }

        function deleteInvoices(invoices) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = invoices.map(function (invoice) {
                    return invoice.id;
                });

                invoiceService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            if (!permissionFactory.get().permissions.searchInvoices) {
                $location.path('/error/403');
                return;
            }

            $scope.gridAdapter = {
                dataSource: invoiceService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/invoice/create"
            };
        };

    });
})(angular.module('project-x.invoice'));