'use strict';

(function (module) {
    module.controller('invoiceDueListController', function ($scope, $location, sessionFactory, invoiceService, viewbag, permissionFactory) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onRowClick($event, invoice) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path("/invoice/details/" + invoice.id);
                    break;
            }
        }

        $scope.init = function () {
            if (!permissionFactory.get().permissions.searchInvoices) {
                $location.path('/error/403');
                return;
            }

            $scope.gridAdapter = {
                dataSource: invoiceService.searchDueInvoices,
                onRowCommand: onRowClick,
                createNewUrl: "#/invoice/create"
            };
        };

    });
})(angular.module('project-x.invoice'));