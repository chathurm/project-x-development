'use strict';

(function (module) {
    module.service('grnService', function (httpFactory, config) {
        this.create = function (item) {
            var url = config.getItemCreateUrl();
            return httpFactory.post(url, item).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.createInventoryItem = function (inventoryItem) {
            var url = config.inventoryItemCreateUrl();
            return httpFactory.post(url, inventoryItem).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.createGRN = function (grn) {
            var url = config.grnCreateUrl();
            return httpFactory.post(url, grn).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getItemById = function (itemId) {
            var url = config.getItemByIdUrl();
            return httpFactory.get(url, itemId).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getInventoryItemById = function (inventoryId) {
            var url = config.getinventoryItemByIdUrl(inventoryId);
            return httpFactory.get(url, inventoryId).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getGRNDetailsById = function (grnId) {
            var url = config.getGRNDetailsByIdUrl(grnId);
            return httpFactory.get(url, grnId).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getAllItems = function () {
            var url = config.getAllItemsUrl();
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchItem = function (keyword) {
            var url = config.getSearchItemUrl();
            return httpFactory.get(url, keyword).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchInventoryItems = function (searchText, index) {
            var url = config.getInventorySearchUrl();
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getAllItemsInTheInventory = function () {
            var url = config.getAllItemsInTheInventoryUrl();
            return httpFactory.get(url).then(function (response) {
                console.log(response.data);
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchGRNItems = function (searchText, index) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var url = config.getSearchGRNUrl();
            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAvailableInventoryItems = function (searchText) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var url = config.getBaseApi() + '/inventory/search/available?searchText=' + searchText;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.update = function (item) {
            var url = config.getUpdateItemUrl();
            return httpFactory.put(url, item).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.updateInventoryItem = function (item) {
            var url = config.getUpdateInventoryItemUrl(item.inventoryId);
            return httpFactory.put(url, item).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.updateGRN = function (grn) {
            var url = config.getUpdateGRNUrl(grn.id);
            return httpFactory.put(url, grn).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.delete = function (itemId) {
            var url = config.getDeleteItemUrl();
            return httpFactory.delete(url, itemId).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getAllSuppliers = function () {
            var url = config.getAllSuppliersUrl();
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.grn'));