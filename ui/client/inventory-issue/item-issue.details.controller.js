'use strict';

(function (module) {
    module.controller('itemIssueDetailsController', function ($scope, $window, $timeout, $route, $templateRequest, $compile, $toaster, $location, $routeParams, viewbag, sessionFactory, itemIssueService, permissionFactory) {

        var itemIssueId = null;
        $scope.showLoading = false;

        $scope.itemIssue = new Entity.InventoryIssue();
        $scope.itemIssue.items = [];

        function getInventoryIssueDetails(itemIssueId) {
            if (viewbag.isExist('itemIssue')) {
                $scope.itemIssue = viewbag.get('itemIssue');
                viewbag.remove('itemIssue');
            } else {
                $scope.showLoading = true;
                itemIssueService.getInventoryIssueById(itemIssueId).then(function (response) {
                    $scope.itemIssue = response.inventoryIssueData ? response.inventoryIssueData[0] : null;
                    $scope.itemIssue.items = response.inventoryIssueItems;
                    $scope.showLoading = false;
                }).catch(function (err) {
                    $scope.showLoading = false;
                    console.log("Error in controller", err);
                });
            }
        }

        $scope.updateDeleteStatus = function (itemIssue) {
            itemIssueService.changeDeleteStatus(itemIssue.id).then(function (result) {
                $toaster.pop({
                    message: itemIssue.code + ' Successfully Deleted',
                    type: 'success'
                });
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.print = function () {
            angular.element('#print').html('');
            $templateRequest("itemIssue/itemIssue.print.html").then(function (html) {
                angular.element('#print').append($compile(html)($scope));
                $timeout(function () {
                    $window.print();
                }, 200);
            });
        };

        $scope.backToList = function () {
            $location.path("/item-issue/list");
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                itemIssueId = $routeParams.itemIssueId ? $routeParams.itemIssueId : null;
                if (itemIssueId) {
                    getInventoryIssueDetails(itemIssueId);
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

    });
})(angular.module('project-x.item-issue'));