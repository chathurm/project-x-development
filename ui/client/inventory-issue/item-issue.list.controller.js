'use strict';

(function (module) {
    module.controller('issueListController', function ($scope, $location, sessionFactory, itemIssueService, viewbag, config) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteInvoices(records);
                    break;
            }
        }

        function onRowClick($event, issue) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path("/item-issue/details/" + encodeURIComponent(issue.id));
                    break;
                case 'delete':
                    deleteInvoices([issue]);
                    break;
            }
        }

        function deleteInvoices(issues) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = issues.map(function (issue) {
                    return issue.id;
                });

                itemIssueService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: itemIssueService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/item-issue/create"
            };
        };

    });
})(angular.module('project-x.item-issue'));