'use strict';

(function (module) {
    module.controller('productListController', function ($scope, $location, sessionFactory, productService, viewbag, config) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteProducts(records);
                    break;
            }
        }

        function onRowClick($event, product) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path("/product/details/" + encodeURIComponent(product.id));
                    break;
                case 'delete':
                    deleteProducts([product]);
                    break;
            }
        }

        function deleteProducts(products) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = products.map(function (product) {
                    return product.id;
                });

                productService.changeDeleteStatus(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: productService.searchAll,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/product/create"
            };
        };

    });
})(angular.module('project-x.product'));