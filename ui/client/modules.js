angular.module('project-x.utils', []);
angular.module('project-x.dashboard', []);
angular.module('project-x.login', []);
angular.module('project-x.customer', []);
angular.module('project-x.user', []);
angular.module('project-x.supplier', []);
angular.module('project-x.employee', []);
angular.module('project-x.line', []);
angular.module('project-x.inventory', []);
angular.module('project-x.item', []);
angular.module('project-x.invoice', []);
angular.module('project-x.product', []);
angular.module('project-x.customer-product', [])
angular.module('project-x.item-issue', []);
angular.module('project-x.cms', []);
angular.module('project-x.vehicle', []);
angular.module('project-x.grn', []);
angular.module('project-x.order-taking', ['project-x.customer-product']);
angular.module('project-x.payment', []);
angular.module('project-x.tracking', []);
angular.module('project-x.stock', []);
angular.module('project-x.item-stock-log', []);

angular.module('project-x.modules-all', [
    'project-x.dashboard',
    'project-x.login',
    'project-x.user',
    'project-x.customer',
    'project-x.supplier',
    'project-x.employee',
    'project-x.line',
    'project-x.inventory',
    'project-x.item',
    'project-x.invoice',
    'project-x.product',
    'project-x.customer-product',
    'project-x.item-issue',
    'project-x.employee',
    'project-x.cms',
    'project-x.vehicle',
    'project-x.grn',
    'project-x.order-taking',
    'project-x.payment',
    'project-x.tracking',
    'project-x.stock',
    'project-x.item-stock-log'
]);
