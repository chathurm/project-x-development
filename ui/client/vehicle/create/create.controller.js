'use strict';

(function (module) {
    module.controller('vehicleCreateController', function ($scope, $routeParams, $route, $location, $filter, $toaster, lookupService, sessionFactory, vehicleService, lineService, viewbag) {

        var vehicleId = null;
        $scope.showLoading = false;
        $scope.isEditing = false;
        $scope.errorMessage = null;
        $scope.showSaveButton = false;

        function fillYears() {
            $scope.years = [];
            var start = new Date().getFullYear();
            for(var i=start; i> 2000; --i){
                $scope.years.push(i);
            }
            $scope.selectedYear = $scope.years[0];
        }

        $scope.setSelectedYear = function () {
            $scope.vehicle.YoM = $scope.selectedYear;
        };

        $scope.save = function (vehicle) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid) {
                hasValidationErrors = true;
            }

            vehicleService.create(vehicle).then(function (response) {
                if (response.success) {
                    vehicle.vehicleId = response.vehicleId;
                    $toaster.pop({
                        message: 'Record has been successfully saved',
                        type: 'success'
                    });
                    $scope.form.$setPristine();
                    $scope.vehicle = new Entity.vehicle();
                } else {
                    $scope.errorMessage = response.message;
                }
                $scope.showLoading = false;
            }).catch(function (ex) {
                console.log("Error in controller", ex);
                $scope.showLoading = false;
            });

        };

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                fillYears();
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.vehicle'));