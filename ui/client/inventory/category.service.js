'use strict';

(function (module) {
    module.service('categoryService', function (httpFactory, config) {

        this.getActiveCategories = function () {
            var url = config.getBaseApi() + "categories/active";
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getActiveSubCategories = function () {
            var url = config.getBaseApi() + "subcategories/active";
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getSubCategoriesForCategory = function (categoryId) {
            var url = config.getBaseApi() + "subcategories/" + encodeURIComponent(categoryId);
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.inventory'));