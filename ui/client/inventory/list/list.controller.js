'use strict';

(function (module) {
    module.controller('inventoryListController', function ($scope, $location, sessionFactory, inventoryService, viewbag, config, permissionFactory) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onActionClick($event, records) {
            switch ($event.command) {
                case 'delete':
                    deleteLines(records);
                    break;
            }
        }

        function onRowClick($event, inventoryItem) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    viewbag.put('inventoryItem', inventoryItem);
                    $location.path("/inventory/details/" + encodeURIComponent(inventoryItem.inventoryId));
                    break;
                case 'edit':
                    resetQueryStrings();
                    $location.path("/inventory/edit/" + encodeURIComponent(inventoryItem.inventoryId));
                    break;
                case 'delete':
                    deleteLines([inventoryItem]);
                    break;
            }
        };

        function editLine(inventoryItem) {
            $event.preventDefault();
            resetQueryStrings();
            viewbag.put('inventoryItem', inventoryItem);
            $location.path("/inventoryItem/edit/" + encodeURIComponent(inventoryItem.id));
        }

        function deleteLines(inventoryItems) {
            if (confirm('Are you sure you want delete this ?')) {
                var selected = inventoryItems.map(function (inventoryItem) {
                    return inventoryItem.id;
                });

                inventoryItemService.delete(selected).then(function () {
                    $scope.gridAdapter.reload();
                }).catch(function (ex) {
                    console.error('Ex :' + ex);
                });
            }
        }

        $scope.init = function () {
            if (!permissionFactory.get().permissions.viewGRN) {
                $location.path('/error/403');
                return;
            }

            $scope.gridAdapter = {
                dataSource: inventoryService.searchInventoryItems,
                onActionCommand: onActionClick,
                onRowCommand: onRowClick,
                createNewUrl: "#/inventory/create"
            };
        };

    });
})(angular.module('project-x.inventory'));