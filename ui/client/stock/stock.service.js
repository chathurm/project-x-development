'use strict';

(function (module) {
    module.service('stockService', function (httpFactory, config) {
        this.searchAvailableStock = function (searchText, index) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var url = config.getBaseApi() + '/stock/search?searchText=' + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.stock'));