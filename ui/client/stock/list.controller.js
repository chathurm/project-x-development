'use strict';

(function (module) {
    module.controller('stockListController', function ($scope, $location, sessionFactory, stockService, viewbag, config, permissionFactory) {
        $scope.gridAdapter = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onRowClick($event, itemStock) {
            $event.preventDefault();
            switch ($event.command) {
                case 'details':
                    resetQueryStrings();
                    $location.path("/item-stock-log/" + encodeURIComponent(itemStock.itemId));
                    break;
            }
        };

        $scope.init = function () {
            $scope.gridAdapter = {
                dataSource: stockService.searchAvailableStock,
                onRowCommand: onRowClick
            };
        };

    });
})(angular.module('project-x.stock'));