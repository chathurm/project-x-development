'use strict';

(function (module) {
    module.controller('orderTakingListController', function ($scope, $routeParams, $route, $location, $toaster, sessionFactory, itemService, categoryService, viewbag) {

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.order-taking'));