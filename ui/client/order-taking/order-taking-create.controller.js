'use strict';

(function (module) {
    module.controller('orderTakingCreateController', function ($scope, $toaster, $routeParams, $q, sessionFactory, customerService, customerProductService, orderTakingService) {
        $scope.customer = null;
        $scope.products = null;
        $scope.compositeItems = null;
        $scope.productItems = null;
        $scope.step = "step1";
        $scope.selectedProduct = null;

        $scope.terms = [];
        $scope.isLoading = false;
        $scope.isLoadingCustomProducts = false;

        var customerId = $routeParams.customerId;

        var trackingStatus = {
            ordered: 1,
            loaded: 2,
            delivered: 3,
            returned: 4
        };
        var isFreeItemAlreadyIssued = false;

        function createTerms() {
            $scope.terms = [];

            for (var i = 0; i < 12; i++) {
                $scope.terms.push({
                    id: (i + 1),
                    name: 'Term ' + (i + 1),
                    items: [],
                    hasFreeItem: false
                });
            }
        }

        function loadCustomerAndProducts() {
            var promises = [];
            var customerPromise = customerService.getByCustomerId(customerId);
            var productPromise = customerProductService.getCustomerProductForCustomer(customerId);

            $scope.isLoading = true;

            promises = $q.all([customerPromise, productPromise]);

            promises.then(function (response) {
                if (angular.isArray(response)) {
                    $scope.customer = response[0];
                    $scope.products = response[1];
                }

                $scope.isLoading = false;
            }).catch(function (ex) {
                showErrorToaster('Unable to load data. Please try again');
                $scope.isLoading = false;
            });
        }

        function loadCustomProductItems(productId, successFn) {
            successFn = successFn || angular.noop;

            $scope.isLoadingCustomProducts = true;

            var promises = [];
            var productItemPromise = orderTakingService.getCustomProductItems(productId);
            var compositeItemDeliveryPromise = orderTakingService.getCompositeItemDeliveryStatus(productId);

            promises = $q.all([productItemPromise, compositeItemDeliveryPromise]);

            promises.then(function (response) {
                if (angular.isArray(response)) {
                    var compositeItems = response[0] ? response[0].compositeItems : [];
                    var compositeItemDeliveryStatus = response[1] ? response[1] : [];

                    $scope.compositeItems = compositeItems;

                    angular.forEach(compositeItemDeliveryStatus, function(cids){
                        var compositeItem = compositeItems.find(function(ci){
                            return ci.id = cids.compositeItemId; 
                        });

                        if(compositeItem){
                            var compositeItemCopy = angular.copy(compositeItem);

                            compositeItemCopy.isFree = cids.isFree;
                            compositeItemCopy.status = cids.status;
                            compositeItemCopy.term = cids.term;
                            compositeItemCopy.trackingId = cids.trackingId;

                            $scope.terms[compositeItemCopy.term - 1].items.push(compositeItemCopy);

                            if (compositeItemCopy.isFree) {
                                isFreeItemAlreadyIssued = true;
                                $scope.terms[compositeItemCopy.term - 1].hasFreeItem = true;
                            }
                        }
                    });

                    successFn();
                    $scope.isLoadingCustomProducts = false;
                }

                $scope.isLoading = false;
            }).catch(function (ex) {
                showErrorToaster('Unable to load data. Please try again');
                $scope.isLoading = false;
            });
        }

        function saveItemInTerm(termId, compositeItem) {
            orderTakingService.addProductItemToTerm(customerId, $scope.selectedProduct.id, termId, compositeItem.id, compositeItem.isFree).then(function () {
            }).catch(function () {
                showErrorToaster('Unable to save item. Please try again');
            });
        };

        function removeItemFromTerm(trackingId, callback) {
            orderTakingService.removeProductItemFromTerm($scope.selectedProduct.productId, trackingId).then(function () {
                callback('success');
            }).catch(function () {
                callback('error');
                showErrorToaster('Unable to remove item. Please try again');
            });
        };

        function showSuccessToaster(message) {
            $toaster.pop({ message: message, type: 'success' });
        }

        function showErrorToaster(message) {
            $toaster.pop({ message: message, type: 'error' });
        }

        function showWarningToaster(message) {
            $toaster.pop({ message: message, type: 'warning' });
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {

                createTerms();
                loadCustomerAndProducts();

            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

        $scope.selectProduct = function (product) {
            if (!product.selected) {
                $scope.selectedProduct = product;

                angular.forEach($scope.products, function (p) {
                    p.selected = (p.productId === product.productId);
                });

                loadCustomProductItems(product.productId);

                $scope.step = "step2";
            }
        };

        $scope.selectAnotherProduct = function () {
            angular.forEach($scope.products, function (p) {
                p.selected = false;
            });

            $scope.selectedProduct = null;
            $scope.compositeItems = null;
            createTerms();
            $scope.step = "step1";
        };

        $scope.onDropProduct = function (event, ui) {
            var element = ui.draggable;
            var compositeItemId = parseInt(element.attr('composite-item-id'));
            var index = ui.droppable.index();
            var term = $scope.terms[index];

            var compositeItem = $scope.compositeItems.find(function (i) {
                return i.id === compositeItemId;
            });

            var newCompositeItem = angular.copy(compositeItem);

            if (!newCompositeItem.isFree) {
                var maxItemCount = term.hasFreeItem ? 2 : 1;
                if (term.items.length === maxItemCount) {
                    event.preventDefault();
                    element.draggable('option', 'revert', true).trigger('mouseup');

                    showWarningToaster('You can`t add multiple items into a term');
                    return;
                }
            } else {
                if (isFreeItemAlreadyIssued) {
                    event.preventDefault();
                    element.draggable('option', 'revert', true).trigger('mouseup');

                    showWarningToaster('This free item already added');
                    return;
                } else {
                    isFreeItemAlreadyIssued = true;
                    term.hasFreeItem = true;
                }
            }

            newCompositeItem.status = trackingStatus.ordered;
            newCompositeItem.term = index + 1;
            term.items.push(newCompositeItem);

            saveItemInTerm(term.id, newCompositeItem);
        }

        $scope.removeItemFromMonth = function (month, compositeItem, index) {
            if (compositeItem.status === trackingStatus.loaded || compositeItem.status === trackingStatus.delivered) {
                showWarningToaster('Unable to remove this item');
                return;
            }

            month.items.splice(index, 1);

            removeItemFromTerm(compositeItem.trackingId, function (status) {
                if (status === 'error') {
                    //reload to current state
                    $scope.selectProduct($scope.selectedProduct);
                }
            });
        };
    });
})(angular.module('project-x.order-taking'));