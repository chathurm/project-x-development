'use strict';

(function (module) {
    module.controller('itemDetailsController', function ($scope, $route, $location, $routeParams, $timeout, $toaster, sessionFactory, itemService, viewbag) {

        $scope.item = null;
        $scope.showLoading = false;
        $scope.showMap = true;

        function getItemDetails(itemId) {
            $scope.showLoading = true;
            itemService.getItemById(itemId).then(function (result) {
                if (result) {
                    $scope.item = result;
                    $scope.showLoading = false;
                } else {
                    $location.path("/item/list");
                }
            }).catch(function (ex) {
                console.log(ex);
                $toaster.pop(
                    {
                        message: 'An error occured, try again',
                        type: 'error'
                    });
                $scope.showLoading = false;
            });
        };

        $scope.changeStatus = function (item) {
            $scope.showLoading = true;
            var isActive = item.isActive === 1 ? 0 : 1;
            itemService.changeActiveStatus(item.id, isActive).then(function (result) {
                $scope.showLoading = false;
                //TODO: change local dataset without reloading
                $toaster.pop(
                    {
                        message: 'Successfully saved the new status',
                        type: 'success'
                    });
                $route.reload();
            }).catch(function (ex) {
                $scope.showLoading = false;
                $toaster.pop(
                    {
                        message: 'An error occured, try again',
                        type: 'error'
                    });
                console.log(ex);
            });
        };

        $scope.updateDeleteStatus = function (item) {
            itemService.changeDeleteStatus(item.id).then(function (result) {
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop(
                    {
                        message: 'An error occured, try again',
                        type: 'error'
                    });
                console.log(ex);
            });
        };

        $scope.editItem = function (item) {
            viewbag.put('item', item);
            $location.path("/item/edit/" + encodeURIComponent(item.id));
        };

        $scope.backToList = function () {
            $location.path("/item/list");
        }

        $scope.init = function () {
            var itemId = decodeURIComponent($routeParams.itemId);
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
                return;
            }
            if (viewbag.isExist('item')) {
                $scope.item = viewbag.get('item');
                viewbag.remove('item');
            } else {
                getItemDetails(itemId);
            }
        };
    });
})(angular.module('project-x.item'));