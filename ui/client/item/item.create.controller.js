'use strict';

(function (module) {
    module.controller('itemCreateController', function ($scope, $routeParams, $route, $location, $toaster, sessionFactory, itemService, categoryService, viewbag) {

        var itemId = null;
        var itemCopy = new Entity.Item();

        $scope.showLoading = false;
        $scope.isEditing = false;
        $scope.errorMessage = null;
        $scope.showSaveButton = false;
        $scope.categories = [];
        $scope.subCategories = [];

        $scope.item = new Entity.Item();


        function getItemDetails(itemId) {
            if (viewbag.isExist('item')) {
                $scope.showSaveButton = true;
                $scope.item = viewbag.get('item');
                if ($scope.item.categoryId) {
                    loadSubCategories($scope.item.categoryId);
                }
                viewbag.remove('item');
            } else {
                $scope.showLoading = true;
                $scope.item = null;
                itemService.getItemById(itemId).then(function (result) {
                    if (result && result !== 'undefined') {
                        $scope.showSaveButton = true;
                        $scope.showLoading = false;
                        $scope.item = result;
                        itemCopy = angular.copy(result);

                        if (result.categoryId) {
                            loadSubCategories(result.categoryId);
                        }
                    } else {
                        $scope.backToList();
                    }
                }).catch(function (ex) {
                    console.log(ex);
                    $scope.showLoading = false;
                });
            }
        }

        function loadCategories() {
            $scope.categories = [];
            categoryService.getActiveCategories().then(function (results) {
                if (results) {
                    $scope.categories = results;
                }
            }).catch(function (exception) {
                console.log(exception);
            });
        }

        function loadSubCategories(categoryId) {
            $scope.subCategories = [];
            categoryService.getSubCategoriesForCategory(categoryId).then(function (results) {
                if (results) {
                    $scope.subCategories = results;
                }
            }).catch(function (exception) {
                console.log(exception);
            });
        }

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        $scope.createNew = function () {
            if ($scope.isEditing) {
                $location.url('/item/create');
            } else {
                $route.reload();
            }
        };

        $scope.setSelectedCategory = function ($event, category) {
            $event.preventDefault();
            $scope.item.categoryId = category.catId;
            $scope.item.categoryName = category.name;
            $scope.item.subCategoryId = null;
            $scope.item.subCategoryName = null;
            loadSubCategories(category.catId);
        };

        $scope.setSelectedSubCategory = function ($event, subCategory) {
            $event.preventDefault();
            $scope.item.subCategoryId = subCategory.subCatId;
            $scope.item.subCategoryName = subCategory.name;
        };

        $scope.resetCategoryDropdown = function ($event) {
            $event.preventDefault();
            $scope.item.categoryId = null;
            $scope.item.categoryName = null;
            $scope.item.subCategoryId = null;
            $scope.item.subCategoryName = null;
        };

        $scope.resetSubCategoryDropdown = function ($event) {
            $event.preventDefault();
            $scope.item.subCategoryId = null;
            $scope.item.subCategoryName = null;
        };

        $scope.backToList = function () {
            $location.url("/item/list");
        };

        $scope.save = function (item) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid) {
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;
                item.isActive = 1;
                if (itemId && $scope.item) {
                    itemService.update(item).then(function (response) {
                        if (response.success) {
                            viewbag.put('item', item);
                            showSuccessToaster('Item changes has been saved');
                        } else {
                            $scope.errorMessage = response.message;
                        }

                        $scope.showLoading = false;
                        $scope.errorMessage = null;
                        $scope.form.$setPristine();

                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                } else {
                    itemService.create(item).then(function (response) {
                        if (response.success) {
                            item.itemId = response.itemId;
                            viewbag.put('item', item);
                            showSuccessToaster('New item has been created');
                        } else {
                            $scope.errorMessage = response.message;
                        }

                        $scope.reset();

                        $scope.showLoading = false;
                    }).catch(function (ex) {
                        console.log("Error in controller", ex);
                        $scope.showLoading = false;
                    });
                }
            }
        };

        $scope.reset = function ($event) {
            if ($event) {
                $event.preventDefault();
            }

            $scope.showLoading = false;
            $scope.errorMessage = null;
            $scope.form.$setPristine();
            $scope.item = angular.copy(itemCopy);
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                loadCategories();
                itemId = $routeParams.itemId ? decodeURIComponent($routeParams.itemId) : null;
                if (itemId) {
                    $scope.isEditing = true;
                    getItemDetails(itemId);
                } else {
                    $scope.showSaveButton = true;
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.item'));