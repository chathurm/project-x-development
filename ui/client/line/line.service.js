'use strict';

(function (module) {
    module.service('lineService', function (httpFactory, config) {

        this.create = function (line) {
            var url = config.lineCreateUrl();
            return httpFactory.post(url, line).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getByLineId = function (lineId) {
            var url = config.getLineByIdUrl(lineId);
            return httpFactory.get(url).then(function (response) {
                return response.data ? response.data[0] : null;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
        /*
        this.changeActiveStatus = function (customerId, isActive) {
            var url = config.getCustomerDeactivateUrl(customerId);
            return httpFactory.put(url, { isActive: isActive }).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeDeleteStatus = function (customerId) {
            var url = config.getCustomerDeleteUrl(customerId);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
        */
        this.update = function (line) {
            var url = config.getLineUpdateUrl(line.id);
            return httpFactory.put(url, line).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.delete = function (list) {
            var url = config.getLineDeleteUrl();
            return httpFactory.post(url, {list : list}).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getActiveLines = function () {
            var url = config.getActiveLines();
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getLineSearchUrl();
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.line'));
