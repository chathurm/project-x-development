'use strict';

(function (module) {
    module.controller('lineDetailsController', function ($scope, $route, $location, $routeParams, sessionFactory, permissionFactory, lineService, viewbag) {

        $scope.line = null;
        $scope.showLoading = false;

        function getLineDetails(lineId) {
            $scope.showLoading = true;
            $scope.line = null;
            lineService.getByLineId(lineId).then(function (result) {
                if (result && result !== 'undefined') {
                    $scope.line = result;
                }
                $scope.showLoading = false;
            }).catch(function (ex) {
                console.log(ex);
                $scope.showLoading = false;
            });
        }

        $scope.editLine = function (line) {
            viewbag.put('line', line);
            $location.path("/line/edit/" + encodeURIComponent(line.id));
        };

        $scope.cancelLineView = function () {
            $location.path("/line/list");
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                var lineId = $routeParams.lineId ? decodeURIComponent($routeParams.lineId) : null;
                if (lineId) {
                    if (!permissionFactory.get().permissions.viewLine) {
                        $location.path('/error/403');
                        return;
                    }
                    
                    getLineDetails(lineId);
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.line'));