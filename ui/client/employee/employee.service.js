'use strict';

(function (module) {
    module.service('employeeService', function (httpFactory, httpExceptionFactory, config) {

        this.create = function (employee) {
            var url = config.getBaseApi() + "employee/create";
            return httpFactory.post(url, employee).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.getEmployeeByEmployeeId = function (employeeId) {
            var url = config.getBaseApi() + "employee/" + encodeURIComponent(employeeId);
            return httpFactory.get(url).then(function (response) {
                return (response.data && response.data.length > 0) ? response.data[0] : null;
            }).catch(handleException);
        };

        this.changeActiveStatus = function (customerId, isActive) {
            var url = config.getBaseApi() + "customer/status/" + encodeURIComponent(customerId);
            return httpFactory.put(url, { isActive: isActive }).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.changeDeleteStatus = function (employeeId) {
            var url = config.getBaseApi() + "employee/delete/" + encodeURIComponent(employeeId);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.update = function (employee) {
            var url = config.getBaseApi() + "employee/update/" + encodeURIComponent(employee.id);
            return httpFactory.put(url, employee).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + "employees/search";
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        this.recentlyCreated = function (size) {
            var url = config.getBaseApi() + "customers/recently-created?size=" + size;
            
            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(handleException);
        };

        function handleException(ex) {
            switch (ex && ex.status) {
                case 401:
                    httpExceptionFactory.unauthorized(ex);
                    break;

                case 403:
                    httpExceptionFactory.forbidden(ex);
                    break;

                case 500:
                    httpExceptionFactory.internalServerError(ex);
                    break;

                default:
                    httpExceptionFactory.unknownError(ex);
                    break;
            }
        }
    });
})(angular.module('project-x.employee'));