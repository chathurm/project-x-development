'use strict';

(function (module) {
    module.service('itemStockLogService', function (httpFactory, config) {
        this.getItemInventory = function (searchText, index, queryParams) {
            searchText = searchText ? encodeURIComponent(searchText) : null;
            var url = config.getBaseApi() + '/inventories/search?searchText=' + searchText + '&itemId=' + queryParams[0] + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.item-stock-log'));