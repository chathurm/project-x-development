'use strict';

(function (module) {
    module.controller('itemStockLogController', function ($scope, $location, $routeParams, $window, sessionFactory, itemStockLogService, viewbag, config, permissionFactory) {
        $scope.gridAdapterInventory = null;
        $scope.itemId = null;

        function resetQueryStrings() {
            $location.search('page', null);
            $location.search('keyword', null);
        }

        function onRowClick($event, inventoryItem) {
            $event.preventDefault();
            switch ($event.command) {
                case 'viewGRN':
                    resetQueryStrings();
                    $window.open("/grn/" + encodeURIComponent(inventoryItem.grnId), '_blank');
                    break;
            }
        };

        $scope.init = function () {
            $scope.itemId = $routeParams.itemId ? decodeURIComponent($routeParams.itemId) : null;
            $scope.gridAdapterInventory = {
                dataSource: itemStockLogService.getItemInventory,
                onRowCommand: onRowClick,
                queryParams: [$scope.itemId]
            };
        };

    });
})(angular.module('project-x.item-stock-log'));