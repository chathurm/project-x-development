'use strict';

(function (module) {
    module.controller('customerProductDetailsController', function ($scope, $route, $toaster, $location, $routeParams, viewbag, sessionFactory, customerProductService) {

        var customerProductId = null;
        $scope.showLoading = false;

        $scope.customerProduct = new Entity.CustomerProduct();

        function getCustomerProductDetails(customerProductId) {
            if (viewbag.isExist('customerProduct')) {
                $scope.customerProduct = viewbag.get('customerProduct');
                console.log($scope.customerProduct);
                viewbag.remove('customerProduct');
            } else {
                $scope.showLoading = true;
                customerProductService.getCustomerProductById(customerProductId).then(function (result) {
                    $scope.customerProduct = result;
                    $scope.showLoading = false;
                }).catch(function (err) {
                    $scope.showLoading = false;
                    console.log("Error in controller", err);
                });
            }
        }

        $scope.updateDeleteStatus = function (customerProduct) {
            var customerProduct = $scope.customerProduct;
            customerProductService.changeDeleteStatus(customerProduct.id).then(function (result) {
                $toaster.pop({
                    message: customerProduct.code + ' Successfully Deleted',
                    type: 'success'
                });
                $scope.backToList();
            }).catch(function (ex) {
                $toaster.pop({
                    message: 'An error occured, try again',
                    type: 'error'
                });
                console.log(ex);
            });
        };

        $scope.backToList = function () {
            $location.path("/custom-product/list");
        }

        $scope.init = function () {
            if (sessionFactory.isAuthenticated()) {
                customerProductId = $routeParams.customerProductId ? decodeURIComponent($routeParams.customerProductId) : null;
                if (customerProductId) {
                    getCustomerProductDetails(customerProductId);
                }
            } else {
                sessionFactory.redirectToLoginPage();
            }
        };

    });
})(angular.module('project-x.customer-product'));