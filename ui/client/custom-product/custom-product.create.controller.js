'use strict';

(function (module) {
    module.controller('customerProductCreateController', function ($scope, $route, $toaster, customerProductService, sessionFactory) {

        $scope.showLoading = false;
        $scope.errorMessage = null;

        $scope.customerProduct = new Entity.CustomerProduct();

        function showSuccessToaster(message) {
            $toaster.pop({
                message: message,
                type: 'success'
            });
        }

        function showErrorToaster(message) {
            $toaster.pop({
                message: message,
                type: 'error'
            });
        }

        $scope.saveChanges = function (customerProduct) {
            var hasValidationErrors = false;
            $scope.errorMessage = null;

            if (!$scope.form.$valid) {
                $scope.form.$setPristine();
                hasValidationErrors = true;
            }

            if (!hasValidationErrors) {
                $scope.showLoading = true;

                customerProductService.create(customerProduct).then(function (response) {
                    if (response.success) {
                        showSuccessToaster('Customer Product has been successfully saved');
                        $route.reload();
                    } else {
                        $scope.errorMessage = response.message;
                    }
                    $scope.showLoading = false;
                }).catch(function (ex) {
                    showErrorToaster('Customer Product has not being saved. Try again');
                    $scope.showLoading = false;
                });
            }
        };

        $scope.productSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.code };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.code + ' - <strong>' + data.text + '</strong></span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {

                });
            }
        };

        $scope.customerSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.customerId, nic: o.nic };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    var template = '<div>'
                        + '<small class="helper-block">' + data.code + '</small>'
                        + '<p>'
                        + '<strong>' + data.text + '</strong><br/>'
                        + (data.nic ? ('<small>(' + data.nic + ')</small>') : '')
                        + '</p>'
                        + '</div>';

                    return $(template);
                }
                return 'Please wait..';
            }
        };

        $scope.empoyeeSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.name, code: o.employeeId };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.code + ' - <strong>' + data.text + '</strong></span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                });
            }
        };

        $scope.linesSearchConfig = {
            processResults: function (data, params, displaySize) {
                var results = data.records || [];
                var index = params.page || 1;

                return {
                    results: results.map(function (o) {
                        return { id: o.id, text: o.route, line: o };
                    }),
                    pagination: {
                        more: (index * displaySize) < data.recordCount
                    }
                };
            },
            template: function (data, element) {
                if (!data.loading) {
                    return $('<span>' + data.line.code + ' - <strong>' + data.text + '</strong></span> - \t');
                }
                return 'Please wait..';
            },
            onSelect: function (event) {
                $scope.$apply(function () {
                });
            }
        };

        $scope.init = function () {
            if (!sessionFactory.isAuthenticated()) {
                sessionFactory.redirectToLoginPage();
            }
        };
    });
})(angular.module('project-x.customer-product'));