(function (module) {
    module.service('customerProductService', function (config, httpFactory) {

        this.create = function (customerProduct) {
            var url = config.getBaseApi() + "customer-product/create";
            return httpFactory.post(url, customerProduct).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getCustomerProductById = function (customerProductId) {
            var url = config.getBaseApi() + "customer-product/" + encodeURIComponent(customerProductId);
            return httpFactory.get(url).then(function (response) {
                return (response.data && response.data.length > 0) ? response.data[0] : null;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getCustomerProductForCustomer = function(customerId){
            var url = config.getCustomreProductForCustomerUrl(customerId);

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.getCustomerProductItemsForProduct = function(productId){
            var url = config.getCustomerProductItemsForProductUrl(productId);

            return httpFactory.get(url).then(function (response) {
                return (response.data && response.data.length > 0) ? response.data[0] : null;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.changeDeleteStatus = function (customerProductId) {
            var url = config.getBaseApi() + "customer-product/delete/" + encodeURIComponent(customerProductId);
            return httpFactory.put(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };

        this.searchAll = function (searchText, index) {
            var url = config.getBaseApi() + "customer-products/search";
            searchText = searchText ? encodeURIComponent(searchText) : null;

            url += "?searchText=" + searchText + "&size=" + config.getPageSize() + "&index=" + index;

            return httpFactory.get(url).then(function (response) {
                return response.data;
            }).catch(function (ex) {
                console.log(ex);
            });
        };
    });
})(angular.module('project-x.customer-product'));