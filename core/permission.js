const Permissions = {
    CUSTOMERS_ADD: 'CUSTOMERS_ADD',
    CUSTOMERS_SEARCH: 'CUSTOMERS_SEARCH',
    CUSTOMERS_EDIT: 'CUSTOMERS_EDIT',
    CUSTOMERS_VIEW_DETAILS: 'CUSTOMERS_VIEW_DETAILS',
    CUSTOMERS_DELETE: 'CUSTOMERS_DELETE',
    CUSTOMERS_CHANGE_STATUS: 'CUSTOMERS_CHANGE_STATUS',
    INVENTORY_ADD: 'INVENTORY_ADD',
    INVENTORY_EDIT: 'INVENTORY_EDIT',
    LINE_ADD: 'LINE_ADD',
    LINE_EDIT: 'LINE_EDIT',
    LINE_VIEW: 'LINE_VIEW',
    LINE_DELETE: 'LINE_DELETE',
    EMPLOYEE_ADD: 'EMPLOYEE_ADD',
    EMPLOYEE_VIEW_DETAILS: 'EMPLOYEE_VIEW_DETAILS',
    EMPLOYEE_EDIT: 'EMPLOYEE_EDIT',
    EMPLOYEE_SEARCH: 'EMPLOYEE_SEARCH',
    EMPLOYEE_DELETE: 'EMPLOYEE_DELETE',
    GRN_ADD: 'GRN_ADD',
    GRN_VIEW_DETAILS: 'GRN_VIEW_DETAILS',
    INVOICE_ADD: 'INVOICE_ADD',
    INVOICE_SEARCH: 'INVOICE_SEARCH',
    INVOICE_VIEW_DETAILS: 'INVOICE_VIEW_DETAILS',
    PAYMENT_ADD: 'PAYMENT_ADD',
    PAYMENT_VIEW_DETAILS: 'PAYMENT_VIEW_DETAILS'
};

module.exports.Permissions = Permissions;

class PermissionManager {
    constructor() {

    }

    static canExecute(code, req, res) {
        if (req.session && req.session.permissions) {
            let accessCode = String(code);
            return (req.session.permissions.indexOf(accessCode) > -1);
        }

        return false;
    }
}

module.exports.PermissionManager = PermissionManager;