var configuration = require('./config.js'),
    logger = require('./logger.js')
    Database = require('./database.js');

class StoreCache {
    constructor() {
        this.stores = [];
    }

    load() {
        return new Promise((resolve, reject) => {
            var db = new Database(configuration.read().db);
            db.open().catch((err) => {
                logger.log(`Unable to load stores. Database connection failed \n ${err}`);
                reject();
            });

            db.query('SELECT * FROM store').then((stores) => {
                this.stores = stores;
                resolve();
            }).catch((err) => {
                logger.log(`Unable to load stores. \n ${err}`);
                reject();
            });

            db.close();
        });
    }

    isValidStore(req, res, next) {
        if (this.stores && this.stores.length > 0) {
            if (req.params.store) {
                let store = this.stores.find((s) => {
                    if (s.name.toLowerCase() === req.params.store.toLowerCase()) {
                        return s;
                    }
                });

                if (store) {
                    next();
                    return;
                }

                res.status(404).send('Url not found');
            }
        } else {
            this.load().then(() => {
                this.isValidStore(req, res, next);
                return;
            }).catch((err) => {
                res.status(404).send('Url not found');
            });
        }
    }
}

module.exports = StoreCache;
