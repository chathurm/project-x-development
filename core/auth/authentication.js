//https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens
/* SSL - https://letsencrypt.org/ */

var jwt = require('jsonwebtoken'),
    configuration = require('../config.js'),
    Database = require('../database.js'),
    HashString = require('../hashstring.js');
    
class Authentication {
    constructor(expressApp) {
        this.app = expressApp;
    }

    init() {

        //authenticate user
        this.app.post('/authentication', (req, res) => {
            var db = new Database(configuration.read().db);
            db.open().catch((err) => {
                res.json({ success: false, message: 'Authentication failed. Unable to connect to database.' });
            });

            let updateLastLoginDate = true;

            db.execute('CALL getUser(?,?,?)', [req.body.store, req.body.username, updateLastLoginDate]).then((recordSet) => {
                if (!recordSet.hasRecords()) {
                    res.json({ success: false, message: 'Authentication failed. User not found.' });
                } else {
                    let user = recordSet.first();

                    var hash = new HashString();

                    if (hash.validateHash(user.password, user.salt, req.body.password)) {
                        let token = jwt.sign({
                            store: { id: user.storeId, name: req.body.store },
                            username: req.body.username,
                            role: user.role,
                            permissions: user.permissions ? user.permissions.split(',') : [],
                        }, configuration.read().auth.secret, {
                                expiresIn: configuration.read().auth.expiresIn // expires in 24 hours
                            });

                        res.json({
                            success: true,
                            message: 'Enjoy your token!',
                            token: token
                        });
                    } else {
                        res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                    }
                }
            }).catch((err) => {
                if (err) throw err;
            });

            db.close();
        });
    }

    authorize(req, res, next) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        // decode token
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token, configuration.read().auth.secret, function (err, session) {
                if (err) {
                    return res.status(401).send({
                        success: false,
                        message: 'Failed to authenticate'
                    });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.session = session;
                    next();
                }
            });

        } else {

            // if there is no token
            // return an error
            return res.status(403).send({
                success: false,
                message: 'Access token not provided'
            });
        }
    }
}

module.exports = Authentication;

